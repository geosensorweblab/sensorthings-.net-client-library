# SensorThings .NET Client Library #

This contains some general information about the SensorThings Client Library, system requirements, and the main steps you need to follow in order to use the library.

### What is this repository for? ###

This client library was developed using .NET Framework 4.5, and it utilizes the OGC SensorThings API to provide access to .NET system functionality. It can be used to develop Internet of Things (IoT) windows and web applications. It mainly supports the sensing profile part of the OGC SensorThings API data model.

### Requirements ###


* .NET Framework 4.5. 
* Microsoft Visual Studio 2013 (or later version). You can use the free Express version.
* The application should be written in C# language.

### How to use the library? ###

1. Start new C# Desktop or Web application in Visual Studio 2013.
2. Right click on the References tab of your application, and then click Add Reference...
3. In the Reference Manager window, click on 'Brows', then navigate to:
OGCSensorThingsClassLibrary > OGCSensorThingsClassLibrary > bin/Debug
4. In the bin/Debug folder you will find two dll files: Newtonsoft.Json.dll and OGCSensorThingsClassLibrary.dll. Add both of them as new references.
5. Add the following in the top of each .cs file where you will use the library classes or methods:

```
#!C#

using OGCSensorThingsClassLibrary;
using Newtonsoft.Json;
using Newtonsoft;
```
Comprehensive library documentation can be found @ http://mro277002.bitbucket.org/SensorThingsClientLibraryDoc.