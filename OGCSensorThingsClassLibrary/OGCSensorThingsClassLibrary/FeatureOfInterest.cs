﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace OGCSensorThingsClassLibrary
{
    /// <summary>
    /// Represents the property of the feature of the phenomenon whose value is being assigned by an observation. Mostly, the FeatureOfInterest is the Location of the Thing. 
    /// </summary>
    public class FeatureOfInterest
    {
        /// <summary>
        /// The system-generated identifier of the FeatureOfInterest entity. It is unique among entities of the same type in the SensorThings service.
        /// </summary>
        public long id { get; set; }
        /// <summary>
        /// The description about the FeatureOfInterest entity.
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// The encoding type of the feature property. Its value is 'application/vnd.geo+json' for the GeoJSON encodingType.
        /// </summary>
        public string encodingType { get; set; }
        /// <summary>
        /// The geometric shape of the feature. It can be 'Point', 'LineString', or 'Polygon'
        /// </summary>
        public string featureType { get; set; }
        /// <summary>
        /// The list of X/Y coordinates that represent the feature shape. Each point requires two numbers (of type Double) corresponding to X and Y coordinates.
        /// </summary>
        public ArrayList featureCoordinates { get; set; }
        /// <summary>
        /// The list of Observations of the corresponding FeatureOfInterest
        /// </summary>
        public ArrayList observations { get; set; }

        /// <summary>
        /// Creates a new instance of the FeatureOfInterest class with the specified id.
        /// </summary>
        /// <param name="id">Referes to the id parameter of the FeatureOfInterest</param>
        public FeatureOfInterest(long id)
        {
            this.id = id;
        }

        /// <summary>
        /// Creates a new instance of the FeatureOfInterest class with the specified description, encodingType, featureType, featureCoordinates, and observations.
        /// </summary>
        /// <param name="description">Referes to the description parameter of the FeatureOfInterest</param>
        /// <param name="encodingType">Referes to the encodingType parameter of the FeatureOfInterest</param>
        /// <param name="featureType">Referes to the featureType parameter of the FeatureOfInterest</param>
        /// <param name="featureCoordinates">Referes to the featureCoordinates parameter of the FeatureOfInterest</param>
        /// <param name="observations">Referes to the observations parameter of the FeatureOfInterest</param>
        public FeatureOfInterest(string description, string encodingType, string featureType, ArrayList featureCoordinates, ArrayList observations = null)
        {
            this.description = description;
            this.encodingType = encodingType;
            this.featureType = featureType;
            this.featureCoordinates = featureCoordinates;
            this.observations = observations;
        }

    }


}
