﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace OGCSensorThingsClassLibrary
{
    /// <summary>
    /// Specifies the phenomenon of an Observation
    /// </summary>
    public class ObservedProperty
    {
        /// <summary>
        /// The system-generated identifier of the ObservedProperty entity. It is unique among entities of the same type in the SensorThings service.
        /// </summary>
        public long id { get; set; }
        /// <summary>
        /// Description about the ObservedProperty entity.
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// The name of the ObservedProperty.
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// The IRI of the ObservedProperty. Dereferencing the IRI should give a definition of the ObservedProperty.
        /// </summary>
        public string definition { get; set; }
        /// <summary>
        /// The list of datastreams associated with the corresponding ObservedProperty.
        /// </summary>
        public ArrayList datastreams { get; set; }

        /// <summary>
        /// Creates a new instance of the ObservedProperty class with the specified id.
        /// </summary>
        /// <param name="id">Referes to the id parameter of the ObservedProperty</param>
        public ObservedProperty(long id)
        {
            this.id = id;
        }

        /// <summary>
        /// Creates a new instance of the ObservedProperty class with the specified name, definition, description, and datastreams list.
        /// </summary>
        /// <param name="name">Referes to the name parameter of the ObservedProperty</param>
        /// <param name="definition">Referes to the definition parameter of the ObservedProperty</param>
        /// <param name="description">Referes to the description parameter of the ObservedProperty</param>
        /// <param name="datastreams">Referes to the datastreams parameter of the ObservedProperty</param>
        public ObservedProperty(string name, string definition, string description, ArrayList datastreams = null)
        {
            this.description = description;
            this.name = name;
            this.definition = definition;
            this.datastreams = datastreams;
        }
    }
}
