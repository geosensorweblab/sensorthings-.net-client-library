﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace OGCSensorThingsClassLibrary
{
    /// <summary>
    /// Provides the last known location of the Thing.
    /// </summary>
    public class Location
    {
        /// <summary>
        /// The system-generated identifier of the Location entity. It is unique among entities of the same type in the SensorThings service.
        /// </summary>
        public long id { get; set; }
        /// <summary>
        /// Short description about the corresponding Location entity.
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// The encoding type of the Location property. Its value is 'application/vnd.geo+json' for the GeoJSON encodingType.
        /// </summary>
        public string encodingType { get; set; }
        /// <summary>
        /// The geometric shape of the location. It can be 'Point', 'LineString', or 'Polygon'
        /// </summary>
        public string locationType { get; set; }
        /// <summary>
        /// The list of X/Y coordinates that represent the locaiton shape. Each point requires two numbers (of type Double) corresponding to X and Y coordinates.
        /// </summary>
        public ArrayList coordinates { get; set; }
        /// <summary>
        /// The correspongind Thing entity associated with the given Location.
        /// </summary>
        public Thing thing { get; set; }

        /// <summary>
        /// Creates a new instance of the Location class with the specified id.
        /// </summary>
        /// <param name="id">Referes to the id parameter of the Location</param>
        public Location(long id)
        {
            this.id = id;
        }

        /// <summary>
        /// Creates a new instance of the Location class with the specified description, encodingType, locationType, coordinates, and Thing
        /// </summary>
        /// <param name="description">Referes to the description parameter of the Location</param>
        /// <param name="encodingType">Referes to the encodingType parameter of the Location</param>
        /// <param name="locationType">Referes to the locationType parameter of the Location</param>
        /// <param name="coordinates">Referes to the coordinates parameter of the Location</param>
        /// <param name="thing">Referes to the thing parameter of the Location</param>
        public Location(string description, string encodingType, string locationType, ArrayList coordinates, Thing thing = null)
        {
            this.description = description;
            this.encodingType = encodingType;
            this.locationType = locationType;
            this.coordinates = coordinates;
            this.thing = thing;
        }
    }

}
