﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace OGCSensorThingsClassLibrary
{
    /// <summary>
    /// Measures or determines the value of a property.
    /// </summary>
    public class Observation
    {
        /// <summary>
        /// The system-generated identifier of the Observation entity. It is unique among entities of the same type in the SensorThings service.
        /// </summary>
        public long id { get; set; }
        /// <summary>
        /// The estimated value of an ObservedProperty from the Observation.
        /// </summary>
        public string result { get; set; }
        /// <summary>
        /// Describes the quality of the result.
        /// </summary>
        public string resultQuality { get; set; }
        /// <summary>
        /// Key-value pairs showing the environmental conditions during the measurement.
        /// </summary>
        public string parameters { get; set; }
        /// <summary>
        /// The time instant or period of when the Observation happens.
        /// </summary>
        public DateTime phenomenonTime { get; set; }
        /// <summary>
        /// The time of when the Observation's result was generated.
        /// </summary>
        public DateTime resultTime { get; set; }
        /// <summary>
        /// The time period during which the result may be used.
        /// </summary>
        public DateTime validTime { get; set; }
        /// <summary>
        /// The Datastream entity that the Observation belongs to.
        /// </summary>
        public Datastream datastream { get; set; }
        /// <summary>
        /// The FeatureOfInterest entity that the Observation belnogs to.
        /// </summary>
        public FeatureOfInterest featureOfInterest { get; set; }

        /// <summary>
        /// Creates a new instance of the Observation class with the specified id.
        /// </summary>
        /// <param name="id">Referes to the id parameter of the Observation</param>
        public Observation(long id)
        {
            this.id = id;
        }

        /// <summary>
        /// Creates a new instance of the Observation class with the specified phenomenonTime, result, resultTime, datastream, featureOfInterest, resultQuality, and validTime.
        /// </summary>
        /// <param name="phenomenonTime">Referes to the phenomenonTime parameter of the Observation</param>
        /// <param name="result">Referes to the result parameter of the Observation</param>
        /// <param name="resultTime">Referes to the resultTime parameter of the Observation</param>
        /// <param name="datastream">Referes to the datastream parameter of the Observation</param>
        /// <param name="featureOfInterest">Referes to the featureOfInterest parameter of the Observation</param>
        /// <param name="resultQuality">Referes to the resultQuality parameter of the Observation</param>
        /// <param name="validTime">Referes to the validTime parameter of the Observation</param>
        /// <param name="parameters">Referes to the parameters parameter of the Observation</param>
        public Observation(DateTime phenomenonTime, string result, DateTime resultTime,
            Datastream datastream, FeatureOfInterest featureOfInterest, string resultQuality = "",
            DateTime validTime = new DateTime(),
            string parameters = "")
        {
            this.phenomenonTime = phenomenonTime;
            this.result = result;
            this.resultTime = resultTime;
            this.datastream = datastream;
            this.featureOfInterest = featureOfInterest;
            this.resultQuality = resultQuality;
            this.validTime = validTime;
            this.parameters = parameters;

        }

    }
}
