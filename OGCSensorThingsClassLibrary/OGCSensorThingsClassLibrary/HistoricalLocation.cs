﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace OGCSensorThingsClassLibrary
{
    /// <summary>
    /// Contains classes that support OGC SensorThings API Sensing Profile services
    /// </summary>
    internal class NamespaceDoc
    {
    }

    /// <summary>
    /// Provides the current (i.e., last known) and previous locations of the Thing with their time.
    /// </summary>
    public class HistoricalLocation
    {
        /// <summary>
        /// The system-generated identifier of the HistoricalLocation entity. It is unique among entities of the same type in the SensorThings service.
        /// </summary>
        public long id { get; set; }
        /// <summary>
        /// The time when the Thing is known at the Location.
        /// </summary>
        public DateTime time { get; set; }
        /// <summary>
        /// The Location of the Thing at the specified time.
        /// </summary>
        public Location location { get; set; }
        /// <summary>
        /// The correspongind Thing entity associated with the given HistoricalLocation.
        /// </summary>
        public Thing thing { get; set; }

        /// <summary>
        /// Creates a new instance of the HistoricalLocation class with the specified id.
        /// </summary>
        /// <param name="id">Referes to the id parameter of the HistoricalLocation</param>
        public HistoricalLocation(long id)
        {
            this.id = id;
        }

        /// <summary>
        /// Creates a new instance of the HistoricalLocation class with the specified time, location, and thing.
        /// </summary>
        /// <param name="time">Referes to the time parameter of the HistoricalLocation</param>
        /// <param name="location">Referes to the location parameter of the HistoricalLocation</param>
        /// <param name="thing">Referes to the thing parameter of the HistoricalLocation</param>
        public HistoricalLocation(DateTime time, Location location, Thing thing)
        {
            this.time = time;
            this.location = location;
            this.thing = thing;
        }


    }

    

}
