﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Net;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft;
using Newtonsoft.Json;


namespace OGCSensorThingsClassLibrary
{
    /// <summary>
    /// Interacts with the SensorThings API services to CREATE, READ, UPDATE, and DELETE IoT data.
    /// </summary>
    public class Service
    {
        /// <summary>
        /// The URL to the service root that specifies the location of the SensorThings service and the version number.
        /// </summary>
        public string serviceRootURL { get; set; }

        /// <summary>
        /// Creates a new instance of the Service class with the specified service root URL.
        /// </summary>
        /// <param name="serviceRootURL">Referes to the serviceRootURL parameter of the Service</param>
        public Service(string serviceRootURL)
        {
            this.serviceRootURL = serviceRootURL;
        }




        //************************* Thing related functions *************************************
        /// <summary>
        /// Get an ArrayList of Thing objects according to the specified skip and top values.
        /// </summary>
        /// <param name="skip">Indicate the number of records to be skiped from the begining of the returned list from the server.</param>
        /// <param name="top">Indicate the maximum number of records to be returned.</param>
        /// <returns>An ArrayList of Thing objects</returns>
        public ArrayList getThings(int skip = 0, int top = 100) //max top is 100 records (this needs to be documented)
        {
            string thingsStr = "";
            try
            {
                thingsStr = getJson(serviceRootURL + "/Things?$skip=" + skip + "&$top=" + top);
            }
            catch (Exception ex)
            {
                return null;
            }

            
            //deserialize things json of the "count" and "value" keys
            var CountValueDefinition = new { count = 0, value = new ArrayList() };
            var thingValObject = JsonConvert.DeserializeAnonymousType(thingsStr, CountValueDefinition);
         
            ArrayList thingsAL = new ArrayList();
            foreach (object val in thingValObject.value)
            {
                //deserialize thing json
                var thingDefinition = new { id = 0 };
                string valthingJson = val.ToString();
                var thingObject = JsonConvert.DeserializeAnonymousType(valthingJson, thingDefinition);

                Thing tempThing = getThingById(thingObject.id);
                thingsAL.Add(tempThing);
            }

            return thingsAL;
        }


        /// <summary>
        /// Get a JSON string of Thing records according to the specified skip, top, expand, select, filter, orderby, and count values.
        /// </summary>
        /// <param name="skip">Indicate the number of records to be skiped from the begining of the returned list from the server.</param>
        /// <param name="top">Indicate the maximum number of records to be returned.</param>
        /// <param name="expand">Indicates the related entities to be represented inline. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2").</param>
        /// <param name="select">Indicates which propeties to be returned explicitly. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2")</param>
        /// <param name="filter">Allows clients to filter a collection of entities that are addressed by a request URL.</param>
        /// <param name="orderby">Specifies the order in which items are returned from the service. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2")</param>
        /// <param name="count">A boolean value that specifies whether the total count of items be returned along with the result or not.</param>
        /// <returns>A JSON string of the requested Thing records.</returns>
        public string getThingsJSON(int skip = 0, int top = 100, string expand = "", string select = "", string filter = "", string orderby = "", bool count = true)     //?$filter=&$count=&$orderby=&$skip=&$top=&$expand=
        {
            string resultString = "";
            string URL_str = serviceRootURL + "/Things?$skip=" + skip + "&$top=" + top + "&$expand=" + expand + "&$filter=" + filter + "&$orderby=" + orderby + "&$count=" + count;
            if (select.Trim() != "")
            {
                URL_str += "&$select=" + select;
            }

            try
            {
                resultString = getJson(URL_str);
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message + " Please, check that you set a valid Server Root URL.";
            }

            return resultString;
        }

        /// <summary>
        /// Get the Thing object of the given id parameter.
        /// </summary>
        /// <param name="id">The id of the requested Thing entity.</param>
        /// <returns>The Thing object of the given id parameter.</returns>
        public Thing getThingById(long id)
        {
            string json = getThingByIdJSON(id);

            // in case of invalid server URL...
            if (json.StartsWith("Error"))
            {
                return null;
            }

            //deserialize Thing json
            var thingDefinition = new { id = 0, description = "", properties = new object() };
            var thingObject = JsonConvert.DeserializeAnonymousType(json, thingDefinition);

            //get json string of Locations, HistoricalLocations, and Datastreams of the provided thing
            string locStr = getJson(serviceRootURL + "/Things(" + id + ")/Locations");
            string hLocStr = getJson(serviceRootURL + "/Things(" + id + ")/HistoricalLocations");
            string dsStr = getJson(serviceRootURL + "/Things(" + id + ")/Datastreams");


            //deserialize Location json of the "count" and "value" keys
            var CountValueDefinition = new { count = 0, value = new ArrayList() };
            var locationValObject = JsonConvert.DeserializeAnonymousType(locStr, CountValueDefinition);

            ArrayList thingLocations = new ArrayList();

            foreach (object val in locationValObject.value)
            {
                //deserialize Location json
                var locationDefinition = new { id = 0, description = "", encodingType = "", location = new object() };
                string valJson = val.ToString();
                var locationObject = JsonConvert.DeserializeAnonymousType(valJson, locationDefinition);

                //deserialize the location parameter json of the Location class
                var locationParameterDefinition = new { coordinates = new ArrayList(), type = "" };
                string locationParameterJson = locationObject.location.ToString();
                var locationParameterObject = JsonConvert.DeserializeAnonymousType(locationParameterJson, locationParameterDefinition);

                //create the location of the given thing
                Location thingLocation = new Location(locationObject.description, locationObject.encodingType, locationParameterObject.type, locationParameterObject.coordinates);
                thingLocation.id = locationObject.id;

                thingLocations.Add(thingLocation);
            }


            ////deserialize Location json
            //var locationDefinition = new { id = 0, description = "", encodingType = "", location = new object() };
            //string valJson = locationValObject.value[0].ToString();
            //var locationObject = JsonConvert.DeserializeAnonymousType(valJson, locationDefinition);

            ////deserialize the location parameter json of the Location class
            //var locationParameterDefinition = new { coordinates = new ArrayList(), type = "" };
            //string locationParameterJson = locationObject.location.ToString();
            //var locationParameterObject = JsonConvert.DeserializeAnonymousType(locationParameterJson, locationParameterDefinition);


            ////create the location of the given thing
            //Location thingLocation = new Location(locationObject.description, locationObject.encodingType, locationParameterObject.type, locationParameterObject.coordinates);
            //thingLocation.id = locationObject.id;


            //deserialize HistoricalLocation json of the "count" and "value" keys
            var historicalLocationValObject = JsonConvert.DeserializeAnonymousType(hLocStr, CountValueDefinition);


            ArrayList thingHistoricalLocation = new ArrayList();

            foreach (object val in historicalLocationValObject.value)
            {
                var historicalLocationDefinition = new { id = 0, time = new DateTime() };
                string valHLocJson = val.ToString();
                var historicalLocationObject = JsonConvert.DeserializeAnonymousType(valHLocJson, historicalLocationDefinition);

                HistoricalLocation hLocation = new HistoricalLocation(historicalLocationObject.time, null, null);  // you can request the whole object here
                hLocation.id = historicalLocationObject.id;
                thingHistoricalLocation.Add(hLocation);
            }

            ArrayList thingDatastreams = new ArrayList();

            //deserialize Datastream json of the "count" and "value" keys
            var datastreamsValObject = JsonConvert.DeserializeAnonymousType(dsStr, CountValueDefinition);

            foreach (object val in datastreamsValObject.value)
            {
                //deserialize datastream
                var datastreamDefinition = new { id = 0, description = "", unitOfMeasurement = new object(), observationType = "", observedArea = new object(), phenomenonTime = new DateTime(), resultTime = new DateTime() };
                string valDSJson = val.ToString();
                var datastreamObject = JsonConvert.DeserializeAnonymousType(valDSJson, datastreamDefinition);

                Datastream ds = new Datastream(datastreamObject.description, datastreamObject.unitOfMeasurement.ToString(),
                datastreamObject.observationType, null, null, null, null, datastreamObject.observedArea as string,
                datastreamObject.phenomenonTime, datastreamObject.resultTime);
                ds.id = datastreamObject.id;
                thingDatastreams.Add(ds);
            }

            Location finalLocation;

            try
            {
                finalLocation = thingLocations[0] as Location;
            } catch (Exception ex)
            {
                finalLocation = null;
            }
           

            //create the thing
            Thing thing = new Thing(thingObject.description, thingObject.properties.ToString(), finalLocation, thingHistoricalLocation, thingDatastreams);
            thing.id = thingObject.id;

            return thing;
        }

        /// <summary>
        /// Get the Thing record of the given id parameter.
        /// </summary>
        /// <param name="id">The id of the requested Thing entity.</param>
        /// <returns>A JSON string of the requested Thing entity.</returns>
        public string getThingByIdJSON(long id)
        {
            string testStr = "";
            try
            {
                testStr = getJson(serviceRootURL + "/Things(" + id + ")");
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message + " Please, check that you set a valid Server Root URL.";
            }

            return testStr;
        }

        /// <summary>
        /// Create New Thing entity in OGC SensorThings database.
        /// </summary>
        /// <param name="description">The description of the Thing entity.</param>
        /// <param name="properties">The properties of the Thing entity.</param>
        /// <param name="locationID">The id of the Location to be associated with the new Thing entity.</param>
        /// <returns>JSON string of the new Thing entity or Error message if new record is not added.</returns>
        public string addNewThing(string description, string properties = "", long locationID = 0)
        {
            string jsonStr = "{\"description\": \"" + description + "\", \"properties\": {" + properties + "}";//, \"Location\":{\"id\": " + locationID + "}}";
            if (locationID > 0)
            {
                jsonStr += ", \"Locations\":[{\"id\": " + locationID + "}]}";
            }
            else
            {
                jsonStr += "}";
            }

            return addNewThingByJSON(jsonStr);
        }

        /// <summary>
        /// Create New Thing entity in OGC SensorThings database.
        /// </summary>
        /// <param name="jsonString">A JSON string describing the new Thing entity to be created.</param>
        /// <returns>JSON string of the new Thing entity or Error message if new record is not added.</returns>
        public string addNewThingByJSON(string jsonString)
        {

            return addRecord(serviceRootURL + "/Things", jsonString);
        }


        /// <summary>
        /// Delete Thing entity from the OGC SensorThings database.
        /// </summary>
        /// <param name="id">The id of the Thing entity to be deleted.</param>
        /// <returns>String showing the status of the delete operation.</returns>
        public string deleteThing(long id)
        {
            string resultStr = "";
            try
            {
                resultStr = deleteRecord(serviceRootURL + "/Things(" + id + ")");
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message + " Please, check that you set a valid Server Root URL.";
            }

            return resultStr;
        }

        /// <summary>
        /// Update the Description of a Thing entity.
        /// </summary>
        /// <param name="id">The id of the Thing enetity to be updated.</param>
        /// <param name="description">The new description of the Thing entity.</param>
        /// <returns>String showing the updated status.</returns>
        public string setThingDescription(long id, string description)
        {
            string descJson = "{\"description\": \"" + description + "\"}";
            return updateRecord(serviceRootURL + "/Things(" + id + ")", descJson);
        }

        /// <summary>
        /// Update the Properties of a Thing entity.
        /// </summary>
        /// <param name="id">The id of the Thing enetity to be updated.</param>
        /// <param name="properties">The new Properties of the Thing entity.</param>
        /// <returns>String showing the updated status.</returns>
        public string setThingProperties(long id, string properties)
        {
            string poperJson = "{\"properties\": {" + properties + "}}";
            return updateRecord(serviceRootURL + "/Things(" + id + ")", poperJson);
        }

        /// <summary>
        /// Update the Location of a Thing entity.
        /// </summary>
        /// <param name="id">The id of the Thing enetity to be updated.</param>
        /// <param name="locationID">The id of the new Location to be associated with the Thing entity.</param>
        /// <returns>String showing the updated status.</returns>
        public string setThingLocation(long id, long locationID)
        {
            string descJson = "{\"Locations\": [{\"id\":" + locationID + "}]}";
            return updateRecord(serviceRootURL + "/Things(" + id + ")", descJson);
        }
        //*****************************************************************************************


        //***************************** Location related funcitons*********************************
        /// <summary>
        /// Get an ArrayList of Location objects according to the specified skip and top values.
        /// </summary>
        /// <param name="skip">Indicate the number of records to be skiped from the begining of the returned list from the server.</param>
        /// <param name="top">Indicate the maximum number of records to be returned.</param>
        /// <returns>An ArrayList of Location objects</returns>
        public ArrayList getLocations(int skip = 0, int top = 100) //max top is 100 records (this needs to be documented)
        {

            string locationsStr = "";
            try
            {
                locationsStr = getJson(serviceRootURL + "/Locations?$skip=" + skip + "&$top=" + top);
            }
            catch (Exception ex)
            {
                return null;
            }

            //deserialize locations json of the "count" and "value" keys
            var CountValueDefinition = new { count = 0, value = new ArrayList() };
            var locationValObject = JsonConvert.DeserializeAnonymousType(locationsStr, CountValueDefinition);

            ArrayList locationsAL = new ArrayList();
            foreach (object val in locationValObject.value)
            {
                //deserialize thing json
                var locationDefinition = new { id = 0 };
                string valLocationJson = val.ToString();
                var locationObject = JsonConvert.DeserializeAnonymousType(valLocationJson, locationDefinition);

                Location tempLocation = getLocationById(locationObject.id);
                locationsAL.Add(tempLocation);
            }

            return locationsAL;
        }


        /// <summary>
        /// Get a JSON string of Location records according to the specified skip, top, expand, select, filter, orderby, and count values.
        /// </summary>
        /// <param name="skip">Indicate the number of records to be skiped from the begining of the returned list from the server.</param>
        /// <param name="top">Indicate the maximum number of records to be returned.</param>
        /// <param name="expand">Indicates the related entities to be represented inline. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2").</param>
        /// <param name="select">Indicates which propeties to be returned explicitly. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2")</param>
        /// <param name="filter">Allows clients to filter a collection of entities that are addressed by a request URL.</param>
        /// <param name="orderby">Specifies the order in which items are returned from the service. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2")</param>
        /// <param name="count">A boolean value that specifies whether the total count of items be returned along with the result or not.</param>
        /// <returns>A JSON string of the requested Location records.</returns>
        public string getLocationsJSON(int skip = 0, int top = 100, string expand = "", string select = "", string filter = "", string orderby = "", bool count = true)     //?$filter=&$count=&$orderby=&$skip=&$top=&$expand=
        {
            string resultString = "";
            string URL_str = serviceRootURL + "/Locations?$skip=" + skip + "&$top=" + top + "&$expand=" + expand + "&$filter=" + filter + "&$orderby=" + orderby + "&$count=" + count;
            if (select.Trim() != "")
            {
                URL_str += "&$select=" + select;
            }

            try
            {
                resultString = getJson(URL_str);
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message + " Please, check that you set a valid Server Root URL.";
            }

            return resultString;
        }

        /// <summary>
        /// Get the Location object of the given id parameter.
        /// </summary>
        /// <param name="id">The id of the requested Location entity.</param>
        /// <returns>The Location object of the given id parameter.</returns>
        public Location getLocationById(long id)
        {
            string json = getLocationByIdJSON(id);

            // in case of invalid server URL...
            if (json.StartsWith("Error"))
            {
                return null;
            }

            //deserialize Location json
            var locationDefinition = new { id = 0, description = "", encodingType = "", location = new object() };
            var locationObject = JsonConvert.DeserializeAnonymousType(json, locationDefinition);

            //deserialize the location parameter json of the Location class
            var locationParameterDefinition = new { coordinates = new ArrayList(), type = "" };
            string locationParameterJson = locationObject.location.ToString();
            var locationParameterObject = JsonConvert.DeserializeAnonymousType(locationParameterJson, locationParameterDefinition);

            //get json string of Locations, HistoricalLocations, and Datastreams of the provided thing
            string thingStr = getJson(serviceRootURL + "/Locations(" + id + ")/Things");
            string hLocStr = getJson(serviceRootURL + "/Locations(" + id + ")/HistoricalLocations");

            //deserialize Thing json of the "count" and "value" keys
            var CountValueDefinition = new { count = 0, value = new ArrayList() };
            var thingValObject = JsonConvert.DeserializeAnonymousType(thingStr, CountValueDefinition);


            //deserialize Thing json
            var thingDefinition = new { id = 0, description = "", properties = new object() };
            var thingObject = JsonConvert.DeserializeAnonymousType(thingValObject.value[0].ToString(), thingDefinition);

            Thing locationThing = new Thing(thingObject.description, thingObject.properties.ToString());
            locationThing.id = thingObject.id;

            Location location = new Location(locationObject.description, locationObject.encodingType, locationParameterObject.type, locationParameterObject.coordinates, locationThing);
            location.id = locationObject.id;



            return location;
        }

        /// <summary>
        /// Get the Location record of the given id parameter.
        /// </summary>
        /// <param name="id">The id of the requested Location entity.</param>
        /// <returns>A JSON string of the requested Location entity.</returns>
        public string getLocationByIdJSON(long id)
        {
            string testStr = "";
            try
            {
                testStr = getJson(serviceRootURL + "/Locations(" + id + ")");
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message + " Please, check that you set a valid Server Root URL.";
            }

            return testStr;
        }
      
        /// <summary>
        /// Create New Location entity in OGC SensorThings database.
        /// </summary>
        /// <param name="encodingType">The encodingType of the Location entity.</param>
        /// <param name="description">The description of the Location entity.</param>
        /// <param name="locationType">The locationType of the Location entity.</param>
        /// <param name="coordinates">The coordinates of the Location entity.</param>
        /// <param name="thingID">The id of the Thing to be associated with the new Location entity.</param>
        /// <returns>JSON string of the new Location entity or Error message if new record is not added.</returns>
        public string addNewLocation(string encodingType, string description, string locationType, ArrayList coordinates, long thingID = 0)
        {
            string jsonStr = "{\"description\": \"" + description + "\", \"encodingType\":\"application/vnd.geo+json\", \"location\": {\"type\":\"" + locationType + "\", \"coordinates\": [";
            
            foreach (double d in coordinates)
            {
                jsonStr += d + ",";
            }

            jsonStr = jsonStr.Substring(0, jsonStr.Length - 1); // to remove the extra comma...
            jsonStr += "]}";


            if (thingID > 0)
            {
                jsonStr += ", \"Things\":[{\"id\": " + thingID + "}]}";
            }
            else
            {
                jsonStr += "}";
            }

            return addNewLocationByJSON(jsonStr);
        }

        /// <summary>
        /// Create New Location entity in OGC SensorThings database.
        /// </summary>
        /// <param name="jsonString">A JSON string describing the new Location entity to be created.</param>
        /// <returns>JSON string of the new Location entity or Error message if new record is not added.</returns>
        public string addNewLocationByJSON(string jsonString)
        {
            return addRecord(serviceRootURL + "/Locations", jsonString);
        }


        /// <summary>
        /// Delete Location entity from the OGC SensorThings database.
        /// </summary>
        /// <param name="id">The id of the Location entity to be deleted.</param>
        /// <returns>String showing the status of the delete operation.</returns>
        public string deleteLocation(long id)
        {
            string resultStr = "";
            try
            {
                resultStr = deleteRecord(serviceRootURL + "/Locations(" + id + ")");
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message + " Please, check that you set a valid Server Root URL.";
            }

            return resultStr;
        }


        //*******************************************************************************************



        //***************************** HistoricalLocation related functions ************************
        /// <summary>
        /// Get an ArrayList of HistoricalLocation objects according to the specified skip and top values.
        /// </summary>
        /// <param name="skip">Indicate the number of records to be skiped from the begining of the returned list from the server.</param>
        /// <param name="top">Indicate the maximum number of records to be returned.</param>
        /// <returns>An ArrayList of HistoricalLocation objects</returns>
        public ArrayList getHistoricalLocations(int skip = 0, int top = 100) //max top is 100 records (this needs to be documented)
        {
            string hlocationsStr = "";
            try
            {
                hlocationsStr = getJson(serviceRootURL + "/HistoricalLocations?$skip=" + skip + "&$top=" + top);
            }
            catch (Exception ex)
            {
                return null;
            }

            //deserialize hlocations json of the "count" and "value" keys
            var CountValueDefinition = new { count = 0, value = new ArrayList() };
            var hlocationValObject = JsonConvert.DeserializeAnonymousType(hlocationsStr, CountValueDefinition);

            ArrayList hlocationsAL = new ArrayList();
            foreach (object val in hlocationValObject.value)
            {
                //deserialize thing json
                var hlocationDefinition = new { id = 0 };
                string valhLocationJson = val.ToString();
                var hlocationObject = JsonConvert.DeserializeAnonymousType(valhLocationJson, hlocationDefinition);

                HistoricalLocation temphLocation = getHistoricalLocationById(hlocationObject.id);
                hlocationsAL.Add(temphLocation);
            }

            return hlocationsAL;
        }


        /// <summary>
        /// Get a JSON string of HistoricalLocation records according to the specified skip, top, expand, select, filter, orderby, and count values.
        /// </summary>
        /// <param name="skip">Indicate the number of records to be skiped from the begining of the returned list from the server.</param>
        /// <param name="top">Indicate the maximum number of records to be returned.</param>
        /// <param name="expand">Indicates the related entities to be represented inline. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2").</param>
        /// <param name="select">Indicates which propeties to be returned explicitly. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2")</param>
        /// <param name="filter">Allows clients to filter a collection of entities that are addressed by a request URL.</param>
        /// <param name="orderby">Specifies the order in which items are returned from the service. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2")</param>
        /// <param name="count">A boolean value that specifies whether the total count of items be returned along with the result or not.</param>
        /// <returns>A JSON string of the requested HistoricalLocation records.</returns>
        public string getHistoricalLocationsJSON(int skip = 0, int top = 100, string expand = "", string select = "", string filter = "", string orderby = "", bool count = true)     //?$filter=&$count=&$orderby=&$skip=&$top=&$expand=
        {
            string resultString = "";
            string URL_str = serviceRootURL + "/HistoricalLocations?$skip=" + skip + "&$top=" + top + "&$expand=" + expand + "&$filter=" + filter + "&$orderby=" + orderby + "&$count=" + count;
            if (select.Trim() != "")
            {
                URL_str += "&$select=" + select;
            }

            try
            {
                resultString = getJson(URL_str);
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message + " Please, check that you set a valid Server Root URL.";
            }

            return resultString;
        }

        /// <summary>
        /// Get the HistoricalLocation object of the given id parameter.
        /// </summary>
        /// <param name="id">The id of the requested HistoricalLocation entity.</param>
        /// <returns>The HistoricalLocation object of the given id parameter.</returns>
        public HistoricalLocation getHistoricalLocationById(long id)
        {
            string json = getHistoricalLocationByIdJSON(id);

            // in case of invalid server URL...
            if (json.StartsWith("Error"))
            {
                return null;
            }

            ///deserialize HistoricalLocation json
            var historicalLocationDefinition = new { id = 0, time = new DateTime() };
            var historicalLocationObject = JsonConvert.DeserializeAnonymousType(json, historicalLocationDefinition);


            //get json string of the Thing associated with the provided HistoricalLocation
            string thingStr = getJson(serviceRootURL + "/HistoricalLocations(" + id + ")/Thing");

            //deserialize Thing json
            var thingDefinition = new { id = 0, description = "", properties = new object() };
            var thingObject = JsonConvert.DeserializeAnonymousType(thingStr, thingDefinition);


            //get json string of the Location associated with the provided HistoricalLocation
            string locationStr = getJson(serviceRootURL + "/HistoricalLocations(" + id + ")/Locations");

            //deserialize Location json of the "count" and "value" keys
            var CountValueDefinition = new { count = 0, value = new ArrayList() };
            var locationValObject = JsonConvert.DeserializeAnonymousType(locationStr, CountValueDefinition);

            //deserialize Location json
            var locationDefinition = new { id = 0, description = "", encodingType = "", location = new object() };
            string valJson = locationValObject.value[0].ToString();
            var locationObject = JsonConvert.DeserializeAnonymousType(valJson, locationDefinition);

            //deserialize the location parameter json of the Location class
            var locationParameterDefinition = new { coordinates = new ArrayList(), type = "" };
            string locationParameterJson = locationObject.location.ToString();
            var locationParameterObject = JsonConvert.DeserializeAnonymousType(locationParameterJson, locationParameterDefinition);


            Thing historicalLocationThing = new Thing(thingObject.description, thingObject.properties.ToString());
            historicalLocationThing.id = thingObject.id;

            Location hLocLocation = new Location(locationObject.description, locationObject.encodingType, locationParameterObject.type, locationParameterObject.coordinates);
            hLocLocation.id = locationObject.id;

            HistoricalLocation hLocation = new HistoricalLocation(historicalLocationObject.time, hLocLocation, historicalLocationThing);
            hLocation.id = historicalLocationObject.id;

            return hLocation;
        }

        /// <summary>
        /// Get the HistoricalLocation record of the given id parameter.
        /// </summary>
        /// <param name="id">The id of the requested HistoricalLocation entity.</param>
        /// <returns>A JSON string of the requested HistoricalLocation entity.</returns>
        public string getHistoricalLocationByIdJSON(long id)
        {
            string testStr = "";
            try
            {
                testStr = getJson(serviceRootURL + "/HistoricalLocations(" + id + ")");
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message + " Please, check that you set a valid Server Root URL.";
            }

            return testStr;
        }

        /// <summary>
        /// Create New HistoricalLocation entity in OGC SensorThings database.
        /// </summary>
        /// <param name="time">The time of the HistoricalLocation entity.</param>
        /// <param name="thingID">The id of the Thing to be associated with the new HistoricalLocation entity.</param>
        /// <param name="locationID">The id of the Location to be associated with the new HistoricalLocation entity.</param>
        /// <returns>JSON string of the new HistoricalLocation entity or Error message if new record is not added.</returns>
        public string addNewHistoricalLocation(DateTime time, long thingID = 0, long locationID = 0)
        {
            string jsonStr = "{\"time\": \"" + time + "\"";


            if (thingID > 0)
            {
                jsonStr += ", \"Thing\":{\"id\": " + thingID + "}";
            }

            if (locationID > 0)
            {
                jsonStr += ", \"Locations\":[{\"id\": " + locationID + "}]}";
            }
            else
            {
                jsonStr += "}";
            }


            return addNewHistoricalLocationByJSON(jsonStr);
        }



        /// <summary>
        /// Create New HistoricalLocation entity in OGC SensorThings database.
        /// </summary>
        /// <param name="jsonString">A JSON string describing the new HistoricalLocation entity to be created.</param>
        /// <returns>JSON string of the new HistoricalLocation entity or Error message if new record is not added.</returns>
        public string addNewHistoricalLocationByJSON(string jsonString)
        {
            return addRecord(serviceRootURL + "/HistoricalLocations", jsonString);
        }



        /// <summary>
        /// Delete HistoricalLocation entity from the OGC SensorThings database.
        /// </summary>
        /// <param name="id">The id of the HistoricalLocation entity to be deleted.</param>
        /// <returns>String showing the status of the delete operation.</returns>
        public string deleteHistoricalLocation(long id)
        {
            string resultStr = "";
            try
            {
                resultStr = deleteRecord(serviceRootURL + "/HistoricalLocations(" + id + ")");
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message + " Please, check that you set a valid Server Root URL.";
            }

            return resultStr;
        }


        //**********************************************************************************************


        //********************************** Sensor related functions *********************************
        /// <summary>
        /// Get an ArrayList of Sensor objects according to the specified skip and top values.
        /// </summary>
        /// <param name="skip">Indicate the number of records to be skiped from the begining of the returned list from the server.</param>
        /// <param name="top">Indicate the maximum number of records to be returned.</param>
        /// <returns>An ArrayList of Sensor objects</returns>
        public ArrayList getSensors(int skip = 0, int top = 100) //max top is 100 records (this needs to be documented)
        {
            string sensorStr = "";
            try
            {
                sensorStr = getJson(serviceRootURL + "/Sensors?$skip=" + skip + "&$top=" + top);
            }
            catch (Exception ex)
            {
                return null;
            }

            //deserialize sensor json of the "count" and "value" keys
            var CountValueDefinition = new { count = 0, value = new ArrayList() };
            var senValObject = JsonConvert.DeserializeAnonymousType(sensorStr, CountValueDefinition);

            ArrayList senAL = new ArrayList();
            foreach (object val in senValObject.value)
            {
                //deserialize thing json
                var senDefinition = new { id = 0 };
                string valsenJson = val.ToString();
                var senObject = JsonConvert.DeserializeAnonymousType(valsenJson, senDefinition);

                Sensor tempSen = getSensorById(senObject.id);
                senAL.Add(tempSen);
            }

            return senAL;
        }


        /// <summary>
        /// Get a JSON string of Sensor records according to the specified skip, top, expand, select, filter, orderby, and count values.
        /// </summary>
        /// <param name="skip">Indicate the number of records to be skiped from the begining of the returned list from the server.</param>
        /// <param name="top">Indicate the maximum number of records to be returned.</param>
        /// <param name="expand">Indicates the related entities to be represented inline. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2").</param>
        /// <param name="select">Indicates which propeties to be returned explicitly. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2")</param>
        /// <param name="filter">Allows clients to filter a collection of entities that are addressed by a request URL.</param>
        /// <param name="orderby">Specifies the order in which items are returned from the service. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2")</param>
        /// <param name="count">A boolean value that specifies whether the total count of items be returned along with the result or not.</param>
        /// <returns>A JSON string of the requested Sensor records.</returns>
        public string getSensorsJSON(int skip = 0, int top = 100, string expand = "", string select = "", string filter = "", string orderby = "", bool count = true)     //?$filter=&$count=&$orderby=&$skip=&$top=&$expand=
        {
            string resultString = "";
            string URL_str = serviceRootURL + "/Sensors?$skip=" + skip + "&$top=" + top + "&$expand=" + expand + "&$filter=" + filter + "&$orderby=" + orderby + "&$count=" + count;
            if (select.Trim() != "")
            {
                URL_str += "&$select=" + select;
            }

            try
            {
                resultString = getJson(URL_str);
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message + " Please, check that you set a valid Server Root URL.";
            }

            return resultString;
        }

        /// <summary>
        /// Get the Sensor object of the given id parameter.
        /// </summary>
        /// <param name="id">The id of the requested Sensor entity.</param>
        /// <returns>The Sensor object of the given id parameter.</returns>
        public Sensor getSensorById(long id)
        {
            string json = getSensorByIdJSON(id);

            // in case of invalid server URL...
            if (json.StartsWith("Error"))
            {
                return null;
            }

            //deserialize Sensor json
            var sensorDefinition = new { id = 0, description = "", metadata = "", encodingType = "" };
            var sensorObject = JsonConvert.DeserializeAnonymousType(json, sensorDefinition);


            //get json string of Datastreams of the provided sensor
            string dsStr = getJson(serviceRootURL + "/Sensors(" + id + ")/Datastreams");

            //deserialize Datastream json of the "count" and "value" keys
            var CountValueDefinition = new { count = 0, value = new ArrayList() };
            var datastreamsValObject = JsonConvert.DeserializeAnonymousType(dsStr, CountValueDefinition);

            ArrayList sensorDatastreams = new ArrayList();

            foreach (object val in datastreamsValObject.value)
            {
                var datastreamDefinition = new { id = 0, description = "", unitOfMeasurement = new object(), observationType = "", observedArea = new object(), phenomenonTime = new DateTime(), resultTime = new DateTime() };
                string valDSJson = val.ToString();
                var datastreamObject = JsonConvert.DeserializeAnonymousType(valDSJson, datastreamDefinition);

                Datastream ds = new Datastream(datastreamObject.description, datastreamObject.unitOfMeasurement.ToString(),
                datastreamObject.observationType, null, null, null, null, datastreamObject.observedArea as string,
                datastreamObject.phenomenonTime, datastreamObject.resultTime);
                ds.id = datastreamObject.id;
                sensorDatastreams.Add(ds);
            }

            //create the sensor
            Sensor sensor = new Sensor(sensorObject.description, sensorObject.encodingType, sensorObject.metadata, sensorDatastreams);
            sensor.id = sensorObject.id;

            return sensor;
        }

        /// <summary>
        /// Get the Sensor record of the given id parameter.
        /// </summary>
        /// <param name="id">The id of the requested Sensor entity.</param>
        /// <returns>A JSON string of the requested Sensor entity.</returns>
        public string getSensorByIdJSON(long id)
        {
            string testStr = "";
            try
            {
                testStr = getJson(serviceRootURL + "/Sensors(" + id + ")");
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message + " Please, check that you set a valid Server Root URL.";
            }

            return testStr;
        }

        
        /// <summary>
        /// Create New Thing Sensor entity in OGC SensorThings database.
        /// </summary>
        /// <param name="description">The description of the Sensor entity.</param>
        /// <param name="encodingType">The encodingType of the Sensor entity.</param>
        /// <param name="metadata">The metadata of the Sensor entity.</param>
        /// <returns>JSON string of the new Sensor entity or Error message if new record is not added.</returns>
        public string addNewSensor(string description, string encodingType, string metadata)
        {
            string jsonStr = "{\"description\": \"" + description + "\", \"encodingType\":\"" + encodingType + "\", \"metadata\":\"" + metadata + "\"}";
            return addNewSensorByJSON(jsonStr);
        }


        /// <summary>
        /// Create New Sensor entity in OGC SensorThings database.
        /// </summary>
        /// <param name="jsonString">A JSON string describing the new Sensor entity to be created.</param>
        /// <returns>JSON string of the new Sensor entity or Error message if new record is not added.</returns>
        public string addNewSensorByJSON(string jsonString)
        {

            return addRecord(serviceRootURL + "/Sensors", jsonString);
        }

        /// <summary>
        /// Delete Sensor entity from the OGC SensorThings database.
        /// </summary>
        /// <param name="id">The id of the Sensor entity to be deleted.</param>
        /// <returns>String showing the status of the delete operation.</returns>
        public string deleteSensor(long id)
        {
            string resultStr = "";
            try
            {
                resultStr = deleteRecord(serviceRootURL + "/Sensors(" + id + ")");
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message + " Please, check that you set a valid Server Root URL.";
            }

            return resultStr;
        }


        /// <summary>
        /// Update the Description of a Sensor entity.
        /// </summary>
        /// <param name="id">The id of the Sensor enetity to be updated.</param>
        /// <param name="description">The new description of the Sensor entity.</param>
        /// <returns>String showing the updated status.</returns>
        public string setSensorDescription(long id, string description)
        {

            string descJson = "{\"description\": \"" + description + "\"}";
            return updateRecord(serviceRootURL + "/Sensors(" + id + ")", descJson);
        }

        /// <summary>
        /// Update the EncodingType of a Sensor entity.
        /// </summary>
        /// <param name="id">The id of the Sensor enetity to be updated.</param>
        /// <param name="encodingType">The new encodingType of the Sensor entity.</param>
        /// <returns>String showing the updated status.</returns>
        public string setSensorEncodingType(long id, string encodingType)
        {
            string encTypeJson = "{\"encodingType\": \"" + encodingType + "\"}";
            return updateRecord(serviceRootURL + "/Sensors(" + id + ")", encTypeJson);
        }

        /// <summary>
        /// Update the Metadata of a Sensor entity.
        /// </summary>
        /// <param name="id">The id of the Sensor enetity to be updated.</param>
        /// <param name="metadata">The new metadata of the Sensor entity.</param>
        /// <returns>String showing the updated status.</returns>
        public string setSensorMetadata(long id, string metadata)
        {
            string metaJson = "{\"metadata\": \"" + metadata + "\"}";
            return updateRecord(serviceRootURL + "/Sensors(" + id + ")", metaJson);
        }

        //************************************************************************************************



        //******************************* Datastream related functions ************************************
        /// <summary>
        /// Get an ArrayList of Datastream objects according to the specified skip and top values.
        /// </summary>
        /// <param name="skip">Indicate the number of records to be skiped from the begining of the returned list from the server.</param>
        /// <param name="top">Indicate the maximum number of records to be returned.</param>
        /// <returns>An ArrayList of Datastream objects</returns>
        public ArrayList getDatastreams(int skip = 0, int top = 100) //max top is 100 records (this needs to be documented)
        {
            string dsStr = "";
            try
            {
                dsStr = getJson(serviceRootURL + "/Datastreams?$skip=" + skip + "&$top=" + top);
            }
            catch (Exception ex)
            {
                return null;
            }


            //deserialize class json of the "count" and "value" keys
            var CountValueDefinition = new { count = 0, value = new ArrayList() };
            var valObject = JsonConvert.DeserializeAnonymousType(dsStr, CountValueDefinition);

            ArrayList targetAL = new ArrayList();
            foreach (object val in valObject.value)
            {
                //deserialize thing json
                var classDefinition = new { id = 0 };
                string valClassJson = val.ToString();
                var classObject = JsonConvert.DeserializeAnonymousType(valClassJson, classDefinition);

                Datastream tempObj = getDatastreamById(classObject.id);
                targetAL.Add(tempObj);
            }

            return targetAL;
        }


        /// <summary>
        /// Get a JSON string of Datastream records according to the specified skip, top, expand, select, filter, orderby, and count values.
        /// </summary>
        /// <param name="skip">Indicate the number of records to be skiped from the begining of the returned list from the server.</param>
        /// <param name="top">Indicate the maximum number of records to be returned.</param>
        /// <param name="expand">Indicates the related entities to be represented inline. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2").</param>
        /// <param name="select">Indicates which propeties to be returned explicitly. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2")</param>
        /// <param name="filter">Allows clients to filter a collection of entities that are addressed by a request URL.</param>
        /// <param name="orderby">Specifies the order in which items are returned from the service. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2")</param>
        /// <param name="count">A boolean value that specifies whether the total count of items be returned along with the result or not.</param>
        /// <returns>A JSON string of the requested Datastream records.</returns>
        public string getDatastreamsJSON(int skip = 0, int top = 100, string expand = "", string select = "", string filter = "", string orderby = "", bool count = true)     //?$filter=&$count=&$orderby=&$skip=&$top=&$expand=
        {
            string resultString = "";
            string URL_str = serviceRootURL + "/Datastreams?$skip=" + skip + "&$top=" + top + "&$expand=" + expand + "&$filter=" + filter + "&$orderby=" + orderby + "&$count=" + count;
            if (select.Trim() != "")
            {
                URL_str += "&$select=" + select;
            }

            try
            {
                resultString = getJson(URL_str);
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message + " Please, check that you set a valid Server Root URL.";
            }

            return resultString;
        }

        /// <summary>
        /// Get the Datastream object of the given id parameter.
        /// </summary>
        /// <param name="id">The id of the requested Datastream entity.</param>
        /// <returns>The Datastream object of the given id parameter.</returns>
        public Datastream getDatastreamById(long id)
        {
            string json = getDatastreamByIdJSON(id);

            // in case of invalid server URL...
            if (json.StartsWith("Error"))
            {
                return null;
            }

            //deserialize Datastream json
            var datastreamDefinition = new { id = 0, description = "", unitOfMeasurement = new object(), observationType = "", observedArea = new object(), phenomenonTime = new DateTime(), resultTime = new DateTime()};
            var datastreamObject = JsonConvert.DeserializeAnonymousType(json, datastreamDefinition);


            //get json string of the ObservedProperty of the provided datastream
            string opStr = getJson(serviceRootURL + "/Datastreams(" + id + ")/ObservedProperty");

            //deserialize ObservedProperty json
            var observedPropertyDefinition = new { id = 0, name = "", description = "", definition = "" };
            var observedPropertyObject = JsonConvert.DeserializeAnonymousType(opStr, observedPropertyDefinition);

            ObservedProperty datastreamObservedProperty = new ObservedProperty(observedPropertyObject.name, observedPropertyObject.definition, observedPropertyObject.description);
            datastreamObservedProperty.id = observedPropertyObject.id;

            //get json string of the Thing of the provided datastream
            string thingStr = getJson(serviceRootURL + "/Datastreams(" + id + ")/Thing");

            //deserialize Thing json
            var thingDefinition = new { id = 0, description = "", properties = new object() };
            var thingObject = JsonConvert.DeserializeAnonymousType(thingStr, thingDefinition);

            Thing datastreamThing = new Thing(thingObject.description, thingObject.properties.ToString());
            datastreamThing.id = thingObject.id;


            //get json string of the Sensor of the provided datastream
            string senStr = getJson(serviceRootURL + "/Datastreams(" + id + ")/Sensor");

            //deserialize Sensor json
            var senDefinition = new { id = 0, description = "", metadata = "", encodingType = "" };
            var sensorObject = JsonConvert.DeserializeAnonymousType(senStr, senDefinition);


            Sensor datastreamSensor = new Sensor(sensorObject.description, sensorObject.encodingType, sensorObject.metadata);
            datastreamSensor.id = sensorObject.id;

            //get json string of the Observations of the provided datastream
            string obsStr = getJson(serviceRootURL + "/Datastreams(" + id + ")/Observations");

            //deserialize count and value keys
            var CountValueDefinition = new { count = 0, value = new ArrayList() };
            var observationsValObject = JsonConvert.DeserializeAnonymousType(obsStr, CountValueDefinition);

            ArrayList datastreamObservations = new ArrayList();

            foreach (object val in observationsValObject.value) //deserialize each observation object and add to arrayList
            {
                var observationDefinition = new { id = 0, result = "", resultQuality = "", parameters = "", phenomenonTime = new DateTime(), resultTime = new DateTime(), validTime = new DateTime() };
                string valObsJson = val.ToString();
                var observationObject = JsonConvert.DeserializeAnonymousType(valObsJson, observationDefinition);

                Observation ob = new Observation(observationObject.phenomenonTime, observationObject.result, observationObject.resultTime,
                null, null, observationObject.resultQuality, observationObject.validTime, observationObject.parameters);
                ob.id = observationObject.id;
                datastreamObservations.Add(ob);
            }



            //create the datastream
            Datastream datastream = new Datastream(datastreamObject.description, datastreamObject.unitOfMeasurement.ToString(),
                datastreamObject.observationType, datastreamThing, datastreamSensor, datastreamObservedProperty,
                datastreamObservations, datastreamObject.observedArea as string, datastreamObject.phenomenonTime,
                datastreamObject.resultTime);

            datastream.id = datastreamObject.id;


            return datastream;
        }

        /// <summary>
        /// Get the Datastream record of the given id parameter.
        /// </summary>
        /// <param name="id">The id of the requested Datastream entity.</param>
        /// <returns>A JSON string of the requested Datastream entity.</returns>
        public string getDatastreamByIdJSON(long id)
        {
            string testStr = "";
            try
            {
                testStr = getJson(serviceRootURL + "/Datastreams(" + id + ")");
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message + " Please, check that you set a valid Server Root URL.";
            }

            return testStr;
        }

        /// <summary>
        /// Get Datastream entities associated with the given Thing entity.
        /// </summary>
        /// <param name="thingId">The id of the Thing entity.</param>
        /// <param name="skip">Indicate the number of records to be skiped from the begining of the returned list from the server.</param>
        /// <param name="top">Indicate the maximum number of records to be returned.</param>
        /// <returns>An ArrayList of the requested Datastream objects.</returns>
        public ArrayList getDatastreamsOfThing(long thingId, int skip = 0, int top = 100)
        {
            string dsStr = "";
            try
            {
                dsStr = getJson(serviceRootURL + "/Things(" + thingId + ")/Datastreams?$skip=" + skip + "&$top=" + top);
            }
            catch (Exception ex)
            {
                return null;
            }


            //deserialize class json of the "count" and "value" keys
            var CountValueDefinition = new { count = 0, value = new ArrayList() };
            var valObject = JsonConvert.DeserializeAnonymousType(dsStr, CountValueDefinition);

            ArrayList targetAL = new ArrayList();
            foreach (object val in valObject.value)
            {
                //deserialize datastream json
                var classDefinition = new { id = 0 };
                string valClassJson = val.ToString();
                var classObject = JsonConvert.DeserializeAnonymousType(valClassJson, classDefinition);

                Datastream tempObj = getDatastreamById(classObject.id);
                targetAL.Add(tempObj);
            }

            return targetAL;
        }

        /// <summary>
        /// Get Datastream entities associated with the given Thing entity.
        /// </summary>
        /// <param name="thingId">The id of the Thing entity.</param>
        /// <param name="skip">Indicate the number of records to be skiped from the begining of the returned list from the server.</param>
        /// <param name="top">Indicate the maximum number of records to be returned.</param>
        /// <param name="expand">Indicates the related entities to be represented inline. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2").</param>
        /// <param name="select">Indicates which propeties to be returned explicitly. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2")</param>
        /// <param name="filter">Allows clients to filter a collection of entities that are addressed by a request URL.</param>
        /// <param name="orderby">Specifies the order in which items are returned from the service. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2")</param>
        /// <param name="count">A boolean value that specifies whether the total count of items be returned along with the result or not.</param>
        /// <returns>A JSON string of the requested Datastream records.</returns>
        public string getDatastreamsOfThingJSON(long thingId, int skip = 0, int top = 100, string expand = "", string select = "", string filter = "", string orderby = "", bool count = true)     //?$filter=&$count=&$orderby=&$skip=&$top=&$expand=
        {
            
            string URL_str = serviceRootURL + "/Things(" + thingId + ")/Datastreams?$skip=" + skip + "&$top=" + top + "&$expand=" + expand + "&$filter=" + filter + "&$orderby=" + orderby + "&$count=" + count;
            if (select.Trim() != "")
            {
                URL_str += "&$select=" + select;
            }

            string resultString = "";
            try
            {
                resultString = getJson(URL_str);
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message + " Please, check that you set a valid Server Root URL.";
            }           

            return resultString;
        }

        /// <summary>
        /// Get Datastream entities associated with the given Sensor entity.
        /// </summary>
        /// <param name="sensorId">The id of the Sensor entity.</param>
        /// <param name="skip">Indicate the number of records to be skiped from the begining of the returned list from the server.</param>
        /// <param name="top">Indicate the maximum number of records to be returned.</param>
        /// <returns>An ArrayList of the requested Datastream objects.</returns>
        public ArrayList getDatastreamsOfSensor(long sensorId, int skip = 0, int top = 100)
        {
            string dsStr = "";
            try
            {
                dsStr = getJson(serviceRootURL + "/Sensors(" + sensorId + ")/Datastreams?$skip=" + skip + "&$top=" + top);
            }
            catch (Exception ex)
            {
                return null;
            }


            //deserialize class json of the "count" and "value" keys
            var CountValueDefinition = new { count = 0, value = new ArrayList() };
            var valObject = JsonConvert.DeserializeAnonymousType(dsStr, CountValueDefinition);

            ArrayList targetAL = new ArrayList();
            foreach (object val in valObject.value)
            {
                //deserialize datastream json
                var classDefinition = new { id = 0 };
                string valClassJson = val.ToString();
                var classObject = JsonConvert.DeserializeAnonymousType(valClassJson, classDefinition);

                Datastream tempObj = getDatastreamById(classObject.id);
                targetAL.Add(tempObj);
            }

            return targetAL;
        }

        /// <summary>
        /// Get Datastream entities associated with the given Sensor entity.
        /// </summary>
        /// <param name="sensorId">The id of the Sensor entity.</param>
        /// <param name="skip">Indicate the number of records to be skiped from the begining of the returned list from the server.</param>
        /// <param name="top">Indicate the maximum number of records to be returned.</param>
        /// <param name="expand">Indicates the related entities to be represented inline. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2").</param>
        /// <param name="select">Indicates which propeties to be returned explicitly. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2")</param>
        /// <param name="filter">Allows clients to filter a collection of entities that are addressed by a request URL.</param>
        /// <param name="orderby">Specifies the order in which items are returned from the service. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2")</param>
        /// <param name="count">A boolean value that specifies whether the total count of items be returned along with the result or not.</param>
        /// <returns>A JSON string of the requested Datastream records.</returns>
        public string getDatastreamsOfSensorJSON(long sensorId, int skip = 0, int top = 100, string expand = "", string select = "", string filter = "", string orderby = "", bool count = true)     //?$filter=&$count=&$orderby=&$skip=&$top=&$expand=
        {

            string URL_str = serviceRootURL + "/Sensors(" + sensorId + ")/Datastreams?$skip=" + skip + "&$top=" + top + "&$expand=" + expand + "&$filter=" + filter + "&$orderby=" + orderby + "&$count=" + count;
            if (select.Trim() != "")
            {
                URL_str += "&$select=" + select;
            }

            string resultString = "";
            try
            {
                resultString = getJson(URL_str);
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message + " Please, check that you set a valid Server Root URL.";
            }

            return resultString;
        }

        /// <summary>
        /// Get Datastream entities associated with the given ObservedProperty entity.
        /// </summary>
        /// <param name="observedPropertyId">The id of the ObservedProperty entity.</param>
        /// <param name="skip">Indicate the number of records to be skiped from the begining of the returned list from the server.</param>
        /// <param name="top">Indicate the maximum number of records to be returned.</param>
        /// <returns>An ArrayList of the requested Datastream objects.</returns>
        public ArrayList getDatastreamsOfObservedProperty(long observedPropertyId, int skip = 0, int top = 100)
        {
            string dsStr = "";
            try
            {
                dsStr = getJson(serviceRootURL + "/ObservedProperties(" + observedPropertyId + ")/Datastreams?$skip=" + skip + "&$top=" + top);
            }
            catch (Exception ex)
            {
                return null;
            }


            //deserialize class json of the "count" and "value" keys
            var CountValueDefinition = new { count = 0, value = new ArrayList() };
            var valObject = JsonConvert.DeserializeAnonymousType(dsStr, CountValueDefinition);

            ArrayList targetAL = new ArrayList();
            foreach (object val in valObject.value)
            {
                //deserialize datastream json
                var classDefinition = new { id = 0 };
                string valClassJson = val.ToString();
                var classObject = JsonConvert.DeserializeAnonymousType(valClassJson, classDefinition);

                Datastream tempObj = getDatastreamById(classObject.id);
                targetAL.Add(tempObj);
            }

            return targetAL;
        }

        /// <summary>
        /// Get Datastream entities associated with the given ObservedProperty entity.
        /// </summary>
        /// <param name="observedPropertyId">The id of the ObservedProperty entity.</param>
        /// <param name="skip">Indicate the number of records to be skiped from the begining of the returned list from the server.</param>
        /// <param name="top">Indicate the maximum number of records to be returned.</param>
        /// <param name="expand">Indicates the related entities to be represented inline. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2").</param>
        /// <param name="select">Indicates which propeties to be returned explicitly. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2")</param>
        /// <param name="filter">Allows clients to filter a collection of entities that are addressed by a request URL.</param>
        /// <param name="orderby">Specifies the order in which items are returned from the service. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2")</param>
        /// <param name="count">A boolean value that specifies whether the total count of items be returned along with the result or not.</param>
        /// <returns>A JSON string of the requested Datastream records.</returns>
        public string getDatastreamsOfObservedPropertyJSON(long observedPropertyId, int skip = 0, int top = 100, string expand = "", string select = "", string filter = "", string orderby = "", bool count = true)     //?$filter=&$count=&$orderby=&$skip=&$top=&$expand=
        {

            string URL_str = serviceRootURL + "/ObservedProperties(" + observedPropertyId + ")/Datastreams?$skip=" + skip + "&$top=" + top + "&$expand=" + expand + "&$filter=" + filter + "&$orderby=" + orderby + "&$count=" + count;
            if (select.Trim() != "")
            {
                URL_str += "&$select=" + select;
            }
            string resultString = "";
            try
            {
                resultString = getJson(URL_str);
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message + " Please, check that you set a valid Server Root URL.";
            }

            return resultString;
        }     

        /// <summary>
        /// Create New Datastream entity in OGC SensorThings database.
        /// </summary>
        /// <param name="description">The description of the Datastream entity.</param>
        /// <param name="unitOfMeasurement">The unitOfMeasurement of the Datastream entity.</param>
        /// <param name="observationType">The observationType of the Datastream entity.</param>
        /// <param name="sensorId">The id of the Sensor to be associated with the new Datastream entity.</param>
        /// <param name="thingId">The id of the Thing to be associated with the new Datastream entity.</param>
        /// <param name="observedPropertyId">The id of the ObservedProperty to be associated with the new Datastream entity.</param>
        /// <returns>JSON string of the new Datastream entity or Error message if new record is not added.</returns>
        public string addNewDatastream(string description, string unitOfMeasurement, string observationType, long sensorId, long thingId, long observedPropertyId)
        {
            string jsonStr = "{\"description\": \"" + description + "\", \"unitOfMeasurement\":" + unitOfMeasurement + ", \"observationType\":\"" + observationType + "\"";

            if (sensorId > 0)
            {
                jsonStr += ", \"Sensor\":{\"id\": " + sensorId + "}";
            }

            if (thingId > 0)
            {
                jsonStr += ", \"Thing\":{\"id\": " + thingId + "}";
            }

            if (observedPropertyId > 0)
            {
                jsonStr += ", \"ObservedProperty\":{\"id\": " + observedPropertyId + "}";
            }

            jsonStr += "}";

            return addNewDatastreamByJSON(jsonStr);
        }

        /// <summary>
        /// Create New Datastream entity in OGC SensorThings database.
        /// </summary>
        /// <param name="jsonString">A JSON string describing the new Datastream entity to be created.</param>
        /// <returns>JSON string of the new Datastream entity or Error message if new record is not added.</returns>
        public string addNewDatastreamByJSON(string jsonString)
        {

            return addRecord(serviceRootURL + "/Datastreams", jsonString);
        }

        /// <summary>
        /// Delete Datastream entity from the OGC SensorThings database.
        /// </summary>
        /// <param name="id">The id of the Datastream entity to be deleted.</param>
        /// <returns>String showing the status of the delete operation.</returns>
        public string deleteDatastream(long id)
        {
            string resultStr = "";
            try
            {
                resultStr = deleteRecord(serviceRootURL + "/Datastreams(" + id + ")");
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message + " Please, check that you set a valid Server Root URL.";
            }

            return resultStr;
        }

        /// <summary>
        /// Update the Description of a Datastream entity.
        /// </summary>
        /// <param name="id">The id of the Datastream enetity to be updated.</param>
        /// <param name="description">The new description of the Datastream entity.</param>
        /// <returns>String showing the updated status.</returns>
        public string setDatastreamDescription(long id, string description)
        {
            string descJson = "{\"description\": \"" + description + "\"}";
            return updateRecord(serviceRootURL + "/Datastreams(" + id + ")", descJson);
        }

        /// <summary>
        /// Update the Unit Of Measurement of a Datastream entity.
        /// </summary>
        /// <param name="id">The id of the Datastream enetity to be updated.</param>
        /// <param name="unitOfMeasurement">The new unitOfMeasurement of the Datastream entity.</param>
        /// <returns>String showing the updated status.</returns>
        public string setDatastreamUnitOfMeasurement(long id, string unitOfMeasurement)
        {
            string descJson = "{\"unitOfMeasurement\": {" + unitOfMeasurement + "}}";
            return updateRecord(serviceRootURL + "/Datastreams(" + id + ")", descJson);
        }

        /// <summary>
        /// Update the Observation Type of a Datastream entity.
        /// </summary>
        /// <param name="id">The id of the Datastream enetity to be updated.</param>
        /// <param name="observationType">The new observationType of the Datastream entity.</param>
        /// <returns>String showing the updated status.</returns>
        public string setDatastreamObservationType(long id, string observationType)
        {
            string descJson = "{\"observationType\": \"" + observationType + "\"}";
            return updateRecord(serviceRootURL + "/Datastreams(" + id + ")", descJson);
        }

        //*******************************************************************************************************



        //********************************** ObservedProperty related functions ***********************************
        /// <summary>
        /// Get an ArrayList of ObservedProperty objects according to the specified skip and top values.
        /// </summary>
        /// <param name="skip">Indicate the number of records to be skiped from the begining of the returned list from the server.</param>
        /// <param name="top">Indicate the maximum number of records to be returned.</param>
        /// <returns>An ArrayList of ObservedProperty objects</returns>
        public ArrayList getObservedProperties(int skip = 0, int top = 100) //max top is 100 records (this needs to be documented)
        {
            string opStr = "";
            try
            {
                opStr = getJson(serviceRootURL + "/ObservedProperties?$skip=" + skip + "&$top=" + top);
            }
            catch (Exception ex)
            {
                return null;
            }


            //deserialize class json of the "count" and "value" keys
            var CountValueDefinition = new { count = 0, value = new ArrayList() };
            var valObject = JsonConvert.DeserializeAnonymousType(opStr, CountValueDefinition);

            ArrayList targetAL = new ArrayList();
            foreach (object val in valObject.value)
            {
                //deserialize thing json
                var classDefinition = new { id = 0 };
                string valClassJson = val.ToString();
                var classObject = JsonConvert.DeserializeAnonymousType(valClassJson, classDefinition);

                ObservedProperty tempObj = getObservedPropertyById(classObject.id);
                targetAL.Add(tempObj);
            }

            return targetAL;
        }


        /// <summary>
        /// Get a JSON string of ObservedProperty records according to the specified skip, top, expand, select, filter, orderby, and count values.
        /// </summary>
        /// <param name="skip">Indicate the number of records to be skiped from the begining of the returned list from the server.</param>
        /// <param name="top">Indicate the maximum number of records to be returned.</param>
        /// <param name="expand">Indicates the related entities to be represented inline. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2").</param>
        /// <param name="select">Indicates which propeties to be returned explicitly. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2")</param>
        /// <param name="filter">Allows clients to filter a collection of entities that are addressed by a request URL.</param>
        /// <param name="orderby">Specifies the order in which items are returned from the service. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2")</param>
        /// <param name="count">A boolean value that specifies whether the total count of items be returned along with the result or not.</param>
        /// <returns>A JSON string of the requested ObservedProperty records.</returns>
        public string getObservedPropertiesJSON(int skip = 0, int top = 100, string expand = "", string select = "", string filter = "", string orderby = "", bool count = true)     //?$filter=&$count=&$orderby=&$skip=&$top=&$expand=
        {
            string resultString = "";
            string URL_str = serviceRootURL + "/ObservedProperties?$skip=" + skip + "&$top=" + top + "&$expand=" + expand + "&$filter=" + filter + "&$orderby=" + orderby + "&$count=" + count;
            if (select.Trim() != "")
            {
                URL_str += "&$select=" + select;
            }

            try
            {
                resultString = getJson(URL_str);
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message + " Please, check that you set a valid Server Root URL.";
            }

            return resultString;
        }

        /// <summary>
        /// Get the ObservedProperty object of the given id parameter.
        /// </summary>
        /// <param name="id">The id of the requested ObservedProperty entity.</param>
        /// <returns>The ObservedProperty object of the given id parameter.</returns>
        public ObservedProperty getObservedPropertyById(long id)
        {

            string json = getObservedPropertyByIdJSON(id);

            // in case of invalid server URL...
            if (json.StartsWith("Error"))
            {
                return null;
            }

            //deserialize ObservedProperty json
            var observedPropertyDefinition = new { id = 0, name = "", description  = "", definition = "" };
            var observedPropertyObject = JsonConvert.DeserializeAnonymousType(json, observedPropertyDefinition);


            //get json string of Datastreams of the provided sensor
            string dsStr = getJson(serviceRootURL + "/ObservedProperties(" + id + ")/Datastreams");

            //deserialize Datastream json of the "count" and "value" keys
            var CountValueDefinition = new { count = 0, value = new ArrayList() };
            var datastreamsValObject = JsonConvert.DeserializeAnonymousType(dsStr, CountValueDefinition);

            ArrayList observedPropertyDatastreams = new ArrayList();

            foreach (object val in datastreamsValObject.value)
            {
                //deserialize Datastream
                var datastreamDefinition = new { id = 0, description = "", unitOfMeasurement = new object(), observationType = "", observedArea = new object(), phenomenonTime = new DateTime(), resultTime = new DateTime() };          
                string valDSJson = val.ToString();
                var datastreamObject = JsonConvert.DeserializeAnonymousType(valDSJson, datastreamDefinition);

                Datastream ds = new Datastream(datastreamObject.description, datastreamObject.unitOfMeasurement.ToString(),
                datastreamObject.observationType, null, null, null, null, datastreamObject.observedArea as string,
                datastreamObject.phenomenonTime, datastreamObject.resultTime);
                ds.id = datastreamObject.id;

                observedPropertyDatastreams.Add(ds);
            }

            //create the observedProperty

            ObservedProperty observedProperty = new ObservedProperty(observedPropertyObject.name, observedPropertyObject.definition, observedPropertyObject.description, observedPropertyDatastreams);
            observedProperty.id = observedPropertyObject.id;


            return observedProperty;
        }

        /// <summary>
        /// Get the ObservedProperty record of the given id parameter.
        /// </summary>
        /// <param name="id">The id of the requested ObservedProperty entity.</param>
        /// <returns>A JSON string of the requested ObservedProperty entity.</returns>
        public string getObservedPropertyByIdJSON(long id)
        {
            string testStr = "";
            try
            {
                testStr = getJson(serviceRootURL + "/ObservedProperties(" + id + ")");
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message + " Please, check that you set a valid Server Root URL.";
            }

            return testStr;
        }
        
        /// <summary>
        /// Create New ObservedProperty entity in OGC SensorThings database.
        /// </summary>
        /// <param name="description">The description of the ObservedProperty entity.</param>
        /// <param name="name">The name of the ObservedProperty entity.</param>
        /// <param name="definition">The definition of the ObservedProperty entity.</param>
        /// <returns>JSON string of the new ObservedProperty entity or Error message if new record is not added.</returns>
        public string addNewObservedProperty(string description, string name, string definition)
        {
            string jsonStr = "{\"description\": \"" + description + "\", \"name\":\"" + name + "\", \"definition\":\"" + definition + "\"}";
            return addNewObservedPropertyByJSON(jsonStr);
        }

        /// <summary>
        /// Create New ObservedProperty entity in OGC SensorThings database.
        /// </summary>
        /// <param name="jsonString">A JSON string describing the new ObservedProperty entity to be created.</param>
        /// <returns>JSON string of the new ObservedProperty entity or Error message if new record is not added.</returns>
        public string addNewObservedPropertyByJSON(string jsonString)
        {

            return addRecord(serviceRootURL + "/ObservedProperties", jsonString);
        }

        /// <summary>
        /// Delete ObservedProperty entity from the OGC SensorThings database.
        /// </summary>
        /// <param name="id">The id of the ObservedProperty entity to be deleted.</param>
        /// <returns>String showing the status of the delete operation.</returns>
        public string deleteObservedProperty(long id)
        {
            string resultStr = "";
            try
            {
                resultStr = deleteRecord(serviceRootURL + "/ObservedProperties(" + id + ")");
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message + " Please, check that you set a valid Server Root URL.";
            }

            return resultStr;
        }

        //********************************************************************************************************



        //****************************** Observations related functions ******************************************
        /// <summary>
        /// Get an ArrayList of Observation objects according to the specified skip and top values.
        /// </summary>
        /// <param name="skip">Indicate the number of records to be skiped from the begining of the returned list from the server.</param>
        /// <param name="top">Indicate the maximum number of records to be returned.</param>
        /// <returns>An ArrayList of Observation objects</returns>
        public ArrayList getObservations(int skip = 0, int top = 100) //max top is 100 records (this needs to be documented)
        {
            string obStr = "";
            try
            {
                obStr = getJson(serviceRootURL + "/Observations?$skip=" + skip + "&$top=" + top);
            }
            catch (Exception ex)
            {
                return null;
            }

            //deserialize class json of the "count" and "value" keys
            var CountValueDefinition = new { count = 0, value = new ArrayList() };
            var valObject = JsonConvert.DeserializeAnonymousType(obStr, CountValueDefinition);

            ArrayList targetAL = new ArrayList();
            foreach (object val in valObject.value)
            {
                //deserialize thing json
                var classDefinition = new { id = 0 };
                string valClassJson = val.ToString();
                var classObject = JsonConvert.DeserializeAnonymousType(valClassJson, classDefinition);

                Observation tempObj = getObservationById(classObject.id);
                targetAL.Add(tempObj);
            }

            return targetAL;
        }


        /// <summary>
        /// Get a JSON string of Observation records according to the specified skip, top, expand, select, filter, orderby, and count values.
        /// </summary>
        /// <param name="skip">Indicate the number of records to be skiped from the begining of the returned list from the server.</param>
        /// <param name="top">Indicate the maximum number of records to be returned.</param>
        /// <param name="expand">Indicates the related entities to be represented inline. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2").</param>
        /// <param name="select">Indicates which propeties to be returned explicitly. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2")</param>
        /// <param name="filter">Allows clients to filter a collection of entities that are addressed by a request URL.</param>
        /// <param name="orderby">Specifies the order in which items are returned from the service. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2")</param>
        /// <param name="count">A boolean value that specifies whether the total count of items be returned along with the result or not.</param>
        /// <returns>A JSON string of the requested Observation records.</returns>
        public string getObservationsJSON(int skip = 0, int top = 100, string expand = "", string select = "", string filter = "", string orderby = "", bool count = true)     //?$filter=&$count=&$orderby=&$skip=&$top=&$expand=
        {
            string resultString = "";
            string URL_str = serviceRootURL + "/Observations?$skip=" + skip + "&$top=" + top + "&$expand=" + expand + "&$filter=" + filter + "&$orderby=" + orderby + "&$count=" + count;
            if (select.Trim() != "")
            {
                URL_str += "&$select=" + select;
            }

            try
            {
                resultString = getJson(URL_str);
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message + " Please, check that you set a valid Server Root URL.";
            }

            return resultString;
        }

        /// <summary>
        /// Get the Observation object of the given id parameter.
        /// </summary>
        /// <param name="id">The id of the requested Observation entity.</param>
        /// <returns>The Observation object of the given id parameter.</returns>
        public Observation getObservationById(long id)
        {

            string json = getObservationByIdJSON(id);

            // in case of invalid server URL...
            if (json.StartsWith("Error"))
            {
                return null;
            }

            //deserialize Observation json
            var observationDefinition = new { id = 0, result = "", resultQuality = "", parameters = "", phenomenonTime = new DateTime(), resultTime = new DateTime(), validTime = new DateTime() };
            var observationObject = JsonConvert.DeserializeAnonymousType(json, observationDefinition);


            //get json string of the Datastream of the provided Observation
            string dsStr = getJson(serviceRootURL + "/Observations(" + id + ")/Datastream");

            //deserialize Datastream json
            var datastreamDefinition = new { id = 0, description = "", unitOfMeasurement = new object(), observationType = "", observedArea = new object(), phenomenonTime = new DateTime(), resultTime = new DateTime() };          
            var datastreamObject = JsonConvert.DeserializeAnonymousType(dsStr, datastreamDefinition);

            Datastream observationDatastream = new Datastream(datastreamObject.description, datastreamObject.unitOfMeasurement.ToString(),
                datastreamObject.observationType, null, null, null, null, datastreamObject.observedArea as string,
                datastreamObject.phenomenonTime, datastreamObject.resultTime);
            observationDatastream.id = datastreamObject.id;


            //get json string of the FeatureOfInterest of the provided Observation
            string foiStr = getJson(serviceRootURL + "/Observations(" + id + ")/FeatureOfInterest");

            //deserialize FeatureOfInterest json
            var featureOfInterestDefinition = new { id = 0, description = "", feature = new object(), encodingType = "" };
            var featureOfInterestObject = JsonConvert.DeserializeAnonymousType(foiStr, featureOfInterestDefinition);

            //deserialize the feature parameter json of the FeatureOfInterest class
            var featureDefinition = new { coordinates = new ArrayList(), type = "" };
            string featureJson = featureOfInterestObject.feature.ToString();
            var featureObject = JsonConvert.DeserializeAnonymousType(featureJson, featureDefinition);


            //FOI instance
            FeatureOfInterest observationFeatureOfInterest = new FeatureOfInterest(featureOfInterestObject.description, featureOfInterestObject.encodingType, featureObject.type, featureObject.coordinates);
            observationFeatureOfInterest.id = featureOfInterestObject.id;


            
            //create the observation instance
            Observation observation = new Observation(observationObject.phenomenonTime, observationObject.result, observationObject.resultTime,
                observationDatastream, observationFeatureOfInterest, observationObject.resultQuality, observationObject.validTime, observationObject.parameters);
            observation.id = observationObject.id;


            return observation;

        }

        /// <summary>
        /// Get the Observation record of the given id parameter.
        /// </summary>
        /// <param name="id">The id of the requested Observation entity.</param>
        /// <returns>A JSON string of the requested Observation entity.</returns>
        public string getObservationByIdJSON(long id)
        {
            string testStr = "";
            try
            {
                testStr = getJson(serviceRootURL + "/Observations(" + id + ")");
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message + " Please, check that you set a valid Server Root URL.";
            }

            return testStr;
        }

       
        /// <summary>
        /// Get Observation entities associated with the given Datastream entity.
        /// </summary>
        /// <param name="datastreamId">The id of the Datastream entity.</param>
        /// <param name="skip">Indicate the number of records to be skiped from the begining of the returned list from the server.</param>
        /// <param name="top">Indicate the maximum number of records to be returned.</param>
        /// <returns>An ArrayList of the requested Observation objects.</returns>
        public ArrayList getObservationsOfDatastream(long datastreamId, int skip = 0, int top = 100)
        {
            string obStr = "";
            try
            {
                obStr = getJson(serviceRootURL + "/Datastreams(" + datastreamId + ")/Observations?$skip=" + skip + "&$top=" + top);
            }
            catch (Exception ex)
            {
                return null;
            }

            //deserialize class json of the "count" and "value" keys
            var CountValueDefinition = new { count = 0, value = new ArrayList() };
            var valObject = JsonConvert.DeserializeAnonymousType(obStr, CountValueDefinition);

            ArrayList targetAL = new ArrayList();
            foreach (object val in valObject.value)
            {
                //deserialize thing json
                var classDefinition = new { id = 0 };
                string valClassJson = val.ToString();
                var classObject = JsonConvert.DeserializeAnonymousType(valClassJson, classDefinition);

                Observation tempObj = getObservationById(classObject.id);
                targetAL.Add(tempObj);
            }

            return targetAL;
        }

        /// <summary>
        /// Get Observation entities associated with the given Datastream entity.
        /// </summary>
        /// <param name="datastreamId">The id of the Datastream entity.</param>
        /// <param name="skip">Indicate the number of records to be skiped from the begining of the returned list from the server.</param>
        /// <param name="top">Indicate the maximum number of records to be returned.</param>
        /// <param name="expand">Indicates the related entities to be represented inline. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2").</param>
        /// <param name="select">Indicates which propeties to be returned explicitly. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2")</param>
        /// <param name="filter">Allows clients to filter a collection of entities that are addressed by a request URL.</param>
        /// <param name="orderby">Specifies the order in which items are returned from the service. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2")</param>
        /// <param name="count">A boolean value that specifies whether the total count of items be returned along with the result or not.</param>
        /// <returns>A JSON string of the requested Observation records.</returns>
        public string getObservationsOfDatastreamJSON(long datastreamId, int skip = 0, int top = 100, string expand = "", string select = "", string filter = "", string orderby = "", bool count = true) 
        {
            string resultString = "";
            string URL_str = serviceRootURL + "/Datastreams(" + datastreamId + ")/Observations?$skip=" + skip + "&$top=" + top + "&$expand=" + expand + "&$filter=" + filter + "&$orderby=" + orderby + "&$count=" + count;
            if (select.Trim() != "")
            {
                URL_str += "&$select=" + select;
            }

            try
            {
                resultString = getJson(URL_str);
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message + " Please, check that you set a valid Server Root URL.";
            }

            return resultString;
        }


        /// <summary>
        /// Get Observation entities associated with the given FeatureOfInterest entity.
        /// </summary>
        /// <param name="featureOfInterestId">The id of the FeatureOfInterest entity.</param>
        /// <param name="skip">Indicate the number of records to be skiped from the begining of the returned list from the server.</param>
        /// <param name="top">Indicate the maximum number of records to be returned.</param>
        /// <returns>An ArrayList of the requested Observation objects.</returns>
        public ArrayList getObservationsOfFeatureOfInterest(long featureOfInterestId, int skip = 0, int top = 100)
        {
            string obStr = "";
            try
            {
                obStr = getJson(serviceRootURL + "/FeaturesOfInterest(" + featureOfInterestId + ")/Observations?$skip=" + skip + "&$top=" + top);
            }
            catch (Exception ex)
            {
                return null;
            }

            //deserialize class json of the "count" and "value" keys
            var CountValueDefinition = new { count = 0, value = new ArrayList() };
            var valObject = JsonConvert.DeserializeAnonymousType(obStr, CountValueDefinition);

            ArrayList targetAL = new ArrayList();
            foreach (object val in valObject.value)
            {
                //deserialize thing json
                var classDefinition = new { id = 0 };
                string valClassJson = val.ToString();
                var classObject = JsonConvert.DeserializeAnonymousType(valClassJson, classDefinition);
                Observation tempObj = getObservationById(classObject.id);
                targetAL.Add(tempObj);
            }

            return targetAL;
        }

        /// <summary>
        /// Get Observation entities associated with the given FeatureOfInterest entity.
        /// </summary>
        /// <param name="featureOfInterestId">The id of the FeatureOfInterest entity.</param>
        /// <param name="skip">Indicate the number of records to be skiped from the begining of the returned list from the server.</param>
        /// <param name="top">Indicate the maximum number of records to be returned.</param>
        /// <param name="expand">Indicates the related entities to be represented inline. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2").</param>
        /// <param name="select">Indicates which propeties to be returned explicitly. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2")</param>
        /// <param name="filter">Allows clients to filter a collection of entities that are addressed by a request URL.</param>
        /// <param name="orderby">Specifies the order in which items are returned from the service. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2")</param>
        /// <param name="count">A boolean value that specifies whether the total count of items be returned along with the result or not.</param>
        /// <returns>A JSON string of the requested FeatureOfInterest records.</returns>
        public string getObservationsOfFeatureOfInterestJSON(long featureOfInterestId, int skip = 0, int top = 100, string expand = "", string select = "", string filter = "", string orderby = "", bool count = true) 
        {
            string resultString = "";
            string URL_str = serviceRootURL + "/FeaturesOfInterest(" + featureOfInterestId + ")/Observations?$skip=" + skip + "&$top=" + top + "&$expand=" + expand + "&$filter=" + filter + "&$orderby=" + orderby + "&$count=" + count;
            if (select.Trim() != "")
            {
                URL_str += "&$select=" + select;
            }

            try
            {
                resultString = getJson(URL_str);
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message + " Please, check that you set a valid Server Root URL.";
            }

            return resultString;
        }

        /// <summary>
        /// Delete Observation entity from the OGC SensorThings database.
        /// </summary>
        /// <param name="id">The id of the Observation entity to be deleted.</param>
        /// <returns>String showing the status of the delete operation.</returns>
        public string deleteObservation(long id)
        {
            string resultStr = "";
            try
            {
                resultStr = deleteRecord(serviceRootURL + "/Observations(" + id + ")");
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message + " Please, check that you set a valid Server Root URL.";
            }

            return resultStr;
        }


        /// <summary>
        /// Create New Observation entity in OGC SensorThings database.
        /// </summary>
        /// <param name="result">The result of the Observation entity.</param>
        /// <param name="phenomenonTime">The result of the Observation entity.</param>
        /// <param name="datastreamID">The id of the Datastream to be associated with the new Observation entity.</param>
        /// <param name="featureOfInterestID">The id of the FeatureOfInterest to be associated with the new Observation entity.</param>
        /// <returns>JSON string of the new Observation entity or Error message if new record is not added.</returns>
        public string addNewObservation(string result, string phenomenonTime, long datastreamID, long featureOfInterestID = 0)
        {
            string jsonStr = "{\"result\": " + result + ", \"phenomenonTime\":\"" + phenomenonTime + "\"";

            if (datastreamID > 0)
            {
                jsonStr += ", \"Datastream\":{\"id\": " + datastreamID + "}";
            }

            if (featureOfInterestID > 0)
            {
                jsonStr += ", \"FeatureOfInterest\":{\"id\": " + featureOfInterestID + "}";
            }

            jsonStr += "}";



            return addNewObservationByJSON(jsonStr);
        }

        /// <summary>
        /// Create New Observation entity in OGC SensorThings database.
        /// </summary>
        /// <param name="jsonString">A JSON string describing the new Observation entity to be created.</param>
        /// <returns>JSON string of the new Observation entity or Error message if new record is not added.</returns>
        public string addNewObservationByJSON(string jsonString)
        {
            return addRecord(serviceRootURL + "/Observations", jsonString);
        }


        //************************************************************************************************


        //************************** FeatureOfInterest related functions **********************************
        /// <summary>
        /// Get an ArrayList of FeatureOfInterest objects according to the specified skip and top values.
        /// </summary>
        /// <param name="skip">Indicate the number of records to be skiped from the begining of the returned list from the server.</param>
        /// <param name="top">Indicate the maximum number of records to be returned.</param>
        /// <returns>An ArrayList of FeatureOfInterest objects</returns>
        public ArrayList getFeaturesOfInterest(int skip = 0, int top = 100) //max top is 100 records (this needs to be documented)
        {
            string testStr = "";
            try
            {
                testStr = getJson(serviceRootURL + "/FeaturesOfInterest?$skip=" + skip + "&$top=" + top);
            }
            catch (Exception ex)
            {
                return null;
            }

            //deserialize class json of the "count" and "value" keys
            var CountValueDefinition = new { count = 0, value = new ArrayList() };
            var valObject = JsonConvert.DeserializeAnonymousType(testStr, CountValueDefinition);

            ArrayList targetAL = new ArrayList();
            foreach (object val in valObject.value)
            {
                //deserialize thing json
                var classDefinition = new { id = 0 };
                string valClassJson = val.ToString();
                var classObject = JsonConvert.DeserializeAnonymousType(valClassJson, classDefinition);

                FeatureOfInterest tempObj = getFeatureOfInterestById(classObject.id);
                targetAL.Add(tempObj);
            }

            return targetAL;
        }


        /// <summary>
        /// Get a JSON string of FeatureOfInterest records according to the specified skip, top, expand, select, filter, orderby, and count values.
        /// </summary>
        /// <param name="skip">Indicate the number of records to be skiped from the begining of the returned list from the server.</param>
        /// <param name="top">Indicate the maximum number of records to be returned.</param>
        /// <param name="expand">Indicates the related entities to be represented inline. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2").</param>
        /// <param name="select">Indicates which propeties to be returned explicitly. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2")</param>
        /// <param name="filter">Allows clients to filter a collection of entities that are addressed by a request URL.</param>
        /// <param name="orderby">Specifies the order in which items are returned from the service. Its value is a comma seperated list of navigation property names (e.g. "parameter1, parameter2")</param>
        /// <param name="count">A boolean value that specifies whether the total count of items be returned along with the result or not.</param>
        /// <returns>A JSON string of the requested FeatureOfInterest records.</returns>
        public string getFeaturesOfInterestJSON(int skip = 0, int top = 100, string expand = "", string select = "", string filter = "", string orderby = "", bool count = true)     //?$filter=&$count=&$orderby=&$skip=&$top=&$expand=
        {
            string resultString = "";
            string URL_str = serviceRootURL + "/FeaturesOfInterest?$skip=" + skip + "&$top=" + top + "&$expand=" + expand + "&$filter=" + filter + "&$orderby=" + orderby + "&$count=" + count;
            if (select.Trim() != "")
            {
                URL_str += "&$select=" + select;
            }

            try
            {
                resultString = getJson(URL_str);
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message + " Please, check that you set a valid Server Root URL.";
            }

            return resultString;
        }

        /// <summary>
        /// Get the FeatureOfInerest object of the given id parameter.
        /// </summary>ById(
        /// <param name="id">The id of the requested FeatureOfInterest entity.</param>
        /// <returns>The FeatureOfInterest object of the given id parameter.</returns>
        public FeatureOfInterest getFeatureOfInterestById(long id)
        {

            string json = getFeatureOfInterestByIdJSON(id);

            // in case of invalid server URL...
            if (json.StartsWith("Error"))
            {
                return null;
            }

            //deserialize FeatureOfInterest json
            var featureOfInterestDefinition = new { id = 0, description = "", feature = new object(), encodingType = "" };
            var featureOfInterestObject = JsonConvert.DeserializeAnonymousType(json, featureOfInterestDefinition);

            //deserialize the feature parameter json of the FeatureOfInterest class
            var featureDefinition = new { coordinates = new ArrayList(), type = "" };
            string featureJson = featureOfInterestObject.feature.ToString();
            var featureObject = JsonConvert.DeserializeAnonymousType(featureJson, featureDefinition);


            //get json string of Observations of the provided FeatureOfInterest
            string obStr = getJson(serviceRootURL + "/FeaturesOfInterest(" + id + ")/Observations");

            //deserialize Observation json of the "count" and "value" keys
            var CountValueDefinition = new { count = 0, value = new ArrayList() };
            var obValObject = JsonConvert.DeserializeAnonymousType(obStr, CountValueDefinition);

            ArrayList featureOfInterestObservations = new ArrayList();

            foreach (object val in obValObject.value)
            {
                var obDefinition = new { id = 0, result = "", resultQuality = "", parameters = "", phenomenonTime = new DateTime(), resultTime = new DateTime(), validTime = new DateTime() };
                string valObJson = val.ToString();
                var observationObject = JsonConvert.DeserializeAnonymousType(valObJson, obDefinition);

                Observation obs = new Observation(observationObject.phenomenonTime, observationObject.result, observationObject.resultTime,
                null, null, observationObject.resultQuality, observationObject.validTime, observationObject.parameters);
                obs.id = observationObject.id;
                featureOfInterestObservations.Add(obs);
            }

            //create the featureOfInterest instance
            FeatureOfInterest featureOfInterest = new FeatureOfInterest(featureOfInterestObject.description, featureOfInterestObject.encodingType, featureObject.type, featureObject.coordinates, featureOfInterestObservations);
            featureOfInterest.id = featureOfInterestObject.id;

            return featureOfInterest;
        }

        /// <summary>
        /// Get the FeatureOfInterest record of the given id parameter.
        /// </summary>
        /// <param name="id">The id of the requested FeatureOfInterest entity.</param>
        /// <returns>A JSON string of the requested FeatureOfInterest entity.</returns>
        public string getFeatureOfInterestByIdJSON(long id)
        {
            string testStr = "";
            try
            {
                testStr = getJson(serviceRootURL + "/FeaturesOfInterest(" + id + ")");
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message + " Please, check that you set a valid Server Root URL.";
            }

            return testStr;
        }


        /// <summary>
        /// Create New FeatureOfInterest entity in OGC SensorThings database.
        /// </summary>
        /// <param name="description">The description of the FeatureOfInterest entity.</param>
        /// <param name="encodingType">The encodingType of the FeatureOfInterest entity.</param>
        /// <param name="feature">The feature of the FeatureOfInterest entity.</param>
        /// <returns>JSON string of the new Observation entity or Error message if new record is not added.</returns>
        public string addNewFeatureOfInterest(string description, string encodingType, string feature)
        {
            string jsonStr = "{\"description\": \"" + description + "\", \"encodingType\": \"" + encodingType + "\", \"feature\":" + feature + "}";
            return addNewFeatureOfInterestByJSON(jsonStr);
        }

        /// <summary>
        /// Create New FeatureOfInterest entity in OGC SensorThings database.
        /// </summary>
        /// <param name="jsonString">A JSON string describing the new FeatureOfInterest entity to be created.</param>
        /// <returns>JSON string of the new FeatureOfInterest entity or Error message if new record is not added.</returns>
        public string addNewFeatureOfInterestByJSON(string jsonString)
        {
            return addRecord(serviceRootURL + "/FeaturesOfInterest", jsonString);
        }

        /// <summary>
        /// Delete FeatureOfInterest entity from the OGC SensorThings database.
        /// </summary>
        /// <param name="id">The id of the FeatureOfInterest entity to be deleted.</param>
        /// <returns>String showing the status of the delete operation.</returns>
        public string deleteFeatureOfInterest(long id)
        {
            string resultStr = "";
            try
            {
                resultStr = deleteRecord(serviceRootURL + "/FeaturesOfInterest(" + id + ")");
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message + " Please, check that you set a valid Server Root URL.";
            }

            return resultStr;
        }

        //*************************************************************************************************

        //***************************** Supportive Functions **********************************************


        /// <summary>
        /// Get the requested IoT entities specified by the given URL.
        /// </summary>
        /// <param name="URL">A URL string specifying the enetities to be returned.</param>
        /// <returns>JSON string representing the returned entities.</returns>
        public string getJson(string URL)   
        {
            var request = (HttpWebRequest)WebRequest.Create(URL);
            request.Method = "GET";
            request.ContentType = "application/json; charset=utf-8";

            var response = (HttpWebResponse)request.GetResponse();
            var jsonStr = "";
            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                jsonStr = sr.ReadToEnd();
            }

            return jsonStr;
        }
        

        /// <summary>
        /// Create new entity in the OGC SensorThings database.
        /// </summary>
        /// <param name="URL">String specifying the entity type of the new record.</param>
        /// <param name="json">JSON string describing the new record to be added.</param>
        /// <returns>The status of adding new record.</returns>
        public string addRecord(string URL, string json)
        {
            var request = (HttpWebRequest)WebRequest.Create(URL);
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";
            byte[] byteArray = Encoding.UTF8.GetBytes(json);
            request.ContentLength = byteArray.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            string rsultStr = "";

            try
            {
                var response = (HttpWebResponse)request.GetResponse();
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    rsultStr = sr.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message;
            }


            return rsultStr;
        }


        /// <summary>
        /// Update the content of an entity.
        /// </summary>
        /// <param name="URL">String specifying the entity to be updated.</param>
        /// <param name="json">JSON string describing the record to be updated.</param>
        /// <returns>String showing the update status.</returns>
        public string updateRecord(string URL, string json)
        {
            var request = (HttpWebRequest)WebRequest.Create(URL);
            request.Method = "PATCH";
            request.ContentType = "application/json; charset=utf-8";
            byte[] byteArray = Encoding.UTF8.GetBytes(json);
            request.ContentLength = byteArray.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            string rsultStr = "";

            try
            {
                var response = (HttpWebResponse)request.GetResponse();
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    rsultStr = sr.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message;
            }


            return rsultStr;
        }

        /// <summary>
        /// Delete entity from the OGC SensorThings database.
        /// </summary>
        /// <param name="URL">A URL string specifying the enetity to be deleted</param>
        /// <returns>String showing the delete status</returns>
        public string deleteRecord(string URL)
        {
            var request = (HttpWebRequest)WebRequest.Create(URL);
            request.Method = "DELETE";
            request.ContentType = "application/json; charset=utf-8";

            var jsonStr = "";

            try
            {
                var response = (HttpWebResponse)request.GetResponse();
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    jsonStr = sr.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message;
            }
            
            if (jsonStr.Trim() == "")
            {
                return "Succeeded: Record is deleted";
            }
            else
            {
                return jsonStr;
            }   
        }


    }
}
