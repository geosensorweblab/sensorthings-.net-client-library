﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace OGCSensorThingsClassLibrary
{
    /// <summary>
    /// Represents an instrument that observes a property or phenomenon with the goal of producing an estimated value of the property.
    /// </summary>
    public class Sensor
    {
        /// <summary>
        /// The system-generated identifier of the Sensor entity. It is unique among entities of the same type in the SensorThings service.
        /// </summary>
        public long id { get; set; }
        /// <summary>
        /// Description about the Sensor entity.
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// The encoding type of the metadata property of the Sensor. It takes one of the following values: 'application/pdf' for PDF encoding type and 'http://www.opengis.net/doc/IS/SensorML/2.0' for SensorML type.
        /// </summary>
        public string encodingType { get; set; }
        /// <summary>
        /// The detailed description of the Sensor or system. The type of the metadata is defined by the encdoingType.
        /// </summary>
        public string metadata { get; set; }
        /// <summary>
        /// The list of datastreams associated with the corresponding Sensor.
        /// </summary>
        public ArrayList datastreams { get; set; }

        /// <summary>
        /// Creates a new instance of the Sensor class with the specified id.
        /// </summary>
        /// <param name="id">Referes to the id parameter of the Sensor</param>
        public Sensor(long id)
        {
            this.id = id;
        }

        /// <summary>
        /// Creates a new instance of the Sensor class with the specified description, encodingType, metadata, and datastreams list.
        /// </summary>
        /// <param name="description">Referes to the description parameter of the Sensor</param>
        /// <param name="encodingType">Referes to the encodingType parameter of the Sensor</param>
        /// <param name="metadata">Referes to the metadata parameter of the Sensor</param>
        /// <param name="datastreams">Referes to the datastreams parameter of the Sensor</param>
        public Sensor(string description, string encodingType, string metadata, ArrayList datastreams = null)
        {
            this.description = description;
            this.encodingType = encodingType;
            this.metadata = metadata;
            this.datastreams = datastreams;
        }

    }

    
}
