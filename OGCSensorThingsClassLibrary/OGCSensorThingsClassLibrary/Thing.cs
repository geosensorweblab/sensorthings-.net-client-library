﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections;


namespace OGCSensorThingsClassLibrary
{
    /// <summary>
    /// Represents an object of the physical world or the information world that is capable of being identified and integrated into communication networks.
    /// </summary>
    public class Thing
    {
        /// <summary>
        /// The system-generated identifier of the Thing entity. It is unique among entities of the same type in the SensorThings service.
        /// </summary>
        public long id { get; set; }
        /// <summary>
        /// Short description of the corresponding Thing entity.
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// A JSON string containing user-annotated properties formatted as key-value pairs.
        /// </summary>
        public string properties { get; set; }
        /// <summary>
        /// The last known Location of the corresponding Thing.
        /// </summary>
        public Location location { get; set; }
        /// <summary>
        /// The list of HistoricalLocations of the corresponding Thing.
        /// </summary>
        public ArrayList historicalLocations { get; set; }
        /// <summary>
        /// The list of datastreams associated with the corresponding Thing.
        /// </summary>
        public ArrayList datastreams { get; set; }

        /// <summary>
        /// Creates a new instance of the Thing class with the specified id.
        /// </summary>
        /// <param name="id">Referes to the id parameter of the Thing</param>
        public Thing(long id)
        {
            this.id = id;
        }

        /// <summary>
        /// Creates a new instance of the Thing class with the specified description, properties, location, historicalLocations, and datastreams list.
        /// </summary>
        /// <param name="description">Referes to the description parameter of the Thing</param>
        /// <param name="properties">Referes to the properties parameter of the Thing</param>
        /// <param name="location">Referes to the location parameter of the Thing</param>
        /// <param name="historicalLocations">Referes to the historicalLocations parameter of the Thing</param>
        /// <param name="datastreams">Referes to the datastreams parameter of the Thing</param>
        public Thing(string description, string properties = "", Location location = null, ArrayList historicalLocations = null, ArrayList datastreams = null)
        {
            this.description = description;
            this.properties = properties;
            this.location = location;
            this.historicalLocations = historicalLocations;
            this.datastreams = datastreams;
        }


    }


}
