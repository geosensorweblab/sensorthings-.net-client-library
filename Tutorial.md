# Tutorial #
This is a tutorial about how to use the SensorThings .NET Client Library. It contains code samples of a number of operations  that can be applied using the library.
### Requirements and Initial Steps ###
Please follow the "Requirements" and "How to use the library" sections in the README.md file.
### Code Samples ###
#### 1. Create a new instance of the Service class with specific Service Root URL
The following line of code is normally comes first before using any library functions. It specifies the SensorThings API server to which you will connect.
```
#!C#
Service srvc = new Service("http://chashuhotpot.sensorup.com/OGCSensorThings/v1.0");
```
#### 2. Retrieve Things in JSON Format
The following example retrieve maximum of 10 records of FeaturesOfInterest skipping the first 5 records, displaying id and description only, and sorting by id. It also, consider ids greater than 10000 and does not display records count. The output is in JSON string format and is written to a previously created TextBox (TextBox1).
```
#!C#
Service srvc = new Service("http://chashuhotpot.sensorup.com/OGCSensorThings/v1.0");
TextBox1.Text = srvc.getFeaturesOfInterestJSON(5, 10, "", "id,description", "id gt '10000'", "id", false);
```
Sample Output:
```
{"value":[
{"description":"Generated using location details: WINTER RIVER AT UNION Weather Station","id":175235},
{"description":"Generated using location details: WINTER RIVER AT UNION Weather Station","id":175237},
{"description":"Generated using location details: WINTER RIVER AT UNION Weather Station","id":175239},
{"description":"Generated using location details: WINTER RIVER NEAR SUFFOLK Weather Station","id":175242},
{"description":"Generated using location details: WINTER RIVER AT UNION Weather Station","id":175245},
{"description":"Generated using location details: WINTER RIVER AT UNION Weather Station","id":175246},
{"description":"Generated using location details: WINTER RIVER AT UNION Weather Station","id":175247},
{"description":"Generated using location details: WINTER RIVER AT UNION Weather Station","id":175248},
{"description":"Generated using location details: WINTER RIVER AT UNION Weather Station","id":175249},
{"description":"Generated using location details: WINTER RIVER AT UNION Weather Station","id":175251}],
"nextLink":"http://chashuhotpot.sensorup.com/OGCSensorThings/v1.0/FeaturesOfInterest?$skip=15&$top=10&$expand=&$filter=id gt '10000'&$orderby=id&$count=False&$select=id,description"}
```
#### 3. Retrieve an ArrayList of Things and Loop:
The following example show how to retrieve an ArrayList of 6 Thing objects skipping the first 2000 Thing records. Then, it loops over each Thing object and prints its id, description, and properties. The output is written to a previously created TextBox (TextBox1).
```
#!C#
Service srvc = new Service("http://chashuhotpot.sensorup.com/OGCSensorThings/v1.0");
ArrayList things = srvc.getThings(2000, 6);
            
foreach (Thing tn in things)
{
    TextBox1.Text += "ID: " + tn.id + ",   Description: " + tn.description + ",   Properties: " + tn.properties + "\n"; 
}
```
Sample Output:
```
ID: 203506,   Description: ANDERSON RIVER BELOW CARNWATH RIVER,   Properties: {
  "stationID": "10NC001"
}
ID: 203503,   Description: FIRTH RIVER NEAR THE MOUTH,   Properties: {
  "stationID": "10MD001"
}
ID: 203500,   Description: MACKENZIE RIVER (NAPOIAK CHANNEL) ABOVE SHALLOW BAY,   Properties: {
  "stationID": "10MC023"
}
ID: 203497,   Description: PEEL RIVER AT FROG CREEK,   Properties: {
  "stationID": "10MC022"
}
ID: 203494,   Description: MACKENZIE RIVER (REINDEER CHANNEL) AT ELLICE ISLAND,   Properties: {
  "stationID": "10MC011"
}
ID: 203491,   Description: MACKENZIE RIVER (OUTFLOW MIDDLE CHANNEL) BELOW LANGLEY ISLAND,   Properties: {
  "stationID": "10MC010"
}
```
#### 4. Retrieve All Observations under a Datastream:
The following example shows a simple way to retrieve all Observations (as Observation objects) under a given Datastream (id 293155). It then loops through each observation and write its id and result to a previously created TextBox (TextBox1).
```
#!C#
Service srvc = new Service("http://chashuhotpot.sensorup.com/OGCSensorThings/v1.0");
ArrayList dsList = srvc.getObservationsOfDatastream(293155);
foreach (Observation obs in dsList)
{
     TextBox1.Text += "Observation ID: " + obs.id + ",  Observation Result: " + obs.result + "\n\n";
}
```
Sample Output:
```
Observation ID: 296909,  Observation Result: 1.293

Observation ID: 296908,  Observation Result: 1.294

Observation ID: 296907,  Observation Result: 1.294

Observation ID: 296906,  Observation Result: 1.294

Observation ID: 296905,  Observation Result: 1.293

Observation ID: 296904,  Observation Result: 1.294

Observation ID: 296903,  Observation Result: 1.294

Observation ID: 296902,  Observation Result: 1.294

Observation ID: 296901,  Observation Result: 1.294

Observation ID: 296900,  Observation Result: 1.294

Observation ID: 296899,  Observation Result: 1.293

Observation ID: 296898,  Observation Result: 1.293
...
```
#### 5. Retrieve all observations from a Thing
This example shows how to retrieve all Observations (as objects) that belong to a certain things. It uses multiple loops to write the required output to a text box.
```
#!C#
//ThingByID>Datastreams>Observations
Service srvc = new Service("http://chashuhotpot.sensorup.com/OGCSensorThings/v1.0");
Thing thing = srvc.getThingById(288786);

foreach (Datastream ds in thing.datastreams)
   {
        Datastream ds2 = srvc.getDatastreamById(ds.id);
        foreach (Observation obs in ds2.observations)
           {
                TextBox1.Text += "Thing: " + thing.id + ", " + thing.description + ",    Datastream id:" + ds.id + ",   Observation id: " + obs.id + ", result: " + obs.result + "\n";

           }
   }
```
Sample Output:
```
Thing: 288786, PINCHER CREEK AT FRONT RANGE ROAD,    Datastream id:293148,   Observation id: 295914, result: 7.756
Thing: 288786, PINCHER CREEK AT FRONT RANGE ROAD,    Datastream id:293148,   Observation id: 295913, result: 7.756
Thing: 288786, PINCHER CREEK AT FRONT RANGE ROAD,    Datastream id:293148,   Observation id: 295912, result: 7.755
Thing: 288786, PINCHER CREEK AT FRONT RANGE ROAD,    Datastream id:293148,   Observation id: 295911, result: 7.756
Thing: 288786, PINCHER CREEK AT FRONT RANGE ROAD,    Datastream id:293148,   Observation id: 295910, result: 7.756
Thing: 288786, PINCHER CREEK AT FRONT RANGE ROAD,    Datastream id:293148,   Observation id: 295909, result: 7.755
Thing: 288786, PINCHER CREEK AT FRONT RANGE ROAD,    Datastream id:293148,   Observation id: 295908, result: 7.756
Thing: 288786, PINCHER CREEK AT FRONT RANGE ROAD,    Datastream id:293148,   Observation id: 295907, result: 7.757
Thing: 288786, PINCHER CREEK AT FRONT RANGE ROAD,    Datastream id:293148,   Observation id: 295906, result: 7.756
Thing: 288786, PINCHER CREEK AT FRONT RANGE ROAD,    Datastream id:293148,   Observation id: 295905, result: 7.756
Thing: 288786, PINCHER CREEK AT FRONT RANGE ROAD,    Datastream id:293148,   Observation id: 295904, result: 7.756
Thing: 288786, PINCHER CREEK AT FRONT RANGE ROAD,    Datastream id:293148,   Observation id: 295903, result: 7.755
Thing: 288786, PINCHER CREEK AT FRONT RANGE ROAD,    Datastream id:293148,   Observation id: 295902, result: 7.757
Thing: 288786, PINCHER CREEK AT FRONT RANGE ROAD,    Datastream id:293148,   Observation id: 295901, result: 7.757
Thing: 288786, PINCHER CREEK AT FRONT RANGE ROAD,    Datastream id:293148,   Observation id: 295900, result: 7.757
Thing: 288786, PINCHER CREEK AT FRONT RANGE ROAD,    Datastream id:293148,   Observation id: 295899, result: 7.756
Thing: 288786, PINCHER CREEK AT FRONT RANGE ROAD,    Datastream id:293148,   Observation id: 295898, result: 7.756
Thing: 288786, PINCHER CREEK AT FRONT RANGE ROAD,    Datastream id:293148,   Observation id: 295897, result: 7.756
Thing: 288786, PINCHER CREEK AT FRONT RANGE ROAD,    Datastream id:293148,   Observation id: 295896, result: 7.756
Thing: 288786, PINCHER CREEK AT FRONT RANGE ROAD,    Datastream id:293148,   Observation id: 295895, result: 7.756
...
```
#### 6. Retrieving Observations of FeaturesOfInterest and their Related Thing Data
This example shows how to retrieve all Observations of certain FeatureOfInterest entities and navigate back to get their related Thing data. Note that a maximum of 3 FeaturesOfInterest records are retrieved and after skipping the first 100 records. The output is written to a text box.
```
#!C#
//FeatureOfInterest >> Observations <-> Datastream <-> Thing
Service srvc = new Service("http://chashuhotpot.sensorup.com/OGCSensorThings/v1.0");
ArrayList FIList = srvc.getFeaturesOfInterest(15, 10);

foreach (FeatureOfInterest feature in FIList)
{
    foreach (Observation ob in feature.observations)
    {
         Observation obs = srvc.getObservationById(ob.id);
         Datastream ds = srvc.getDatastreamById(obs.datastream.id);
         TextBox1.Text += "FIO_ID: " + val.id + ", " + "FIO_Description: " + val.description + ", Observation :" + ob.result + ", DS_ID :" + ds.id + ", Thing_ID :" + ds.thing.id + "\n";
    }
}
```
Sample Output:
```
FIO_ID: 283175, FIO_Description: Generated using location details: EMBARRAS RIVER BREAKTHROUGH TO MAMAWI LAKE Weather Station, Observation :6.398, DS_ID :205126, Thing_ID :202153
FIO_ID: 283175, FIO_Description: Generated using location details: EMBARRAS RIVER BREAKTHROUGH TO MAMAWI LAKE Weather Station, Observation :6.396, DS_ID :205126, Thing_ID :202153
FIO_ID: 283175, FIO_Description: Generated using location details: EMBARRAS RIVER BREAKTHROUGH TO MAMAWI LAKE Weather Station, Observation :6.398, DS_ID :205126, Thing_ID :202153
FIO_ID: 283175, FIO_Description: Generated using location details: EMBARRAS RIVER BREAKTHROUGH TO MAMAWI LAKE Weather Station, Observation :6.398, DS_ID :205126, Thing_ID :202153
FIO_ID: 283078, FIO_Description: Generated using location details: MAMAWI LAKE CHANNEL AT OLD DOG CAMP Weather Station, Observation :4.449, DS_ID :205125, Thing_ID :202150
FIO_ID: 283078, FIO_Description: Generated using location details: MAMAWI LAKE CHANNEL AT OLD DOG CAMP Weather Station, Observation :4.450, DS_ID :205125, Thing_ID :202150
FIO_ID: 283078, FIO_Description: Generated using location details: MAMAWI LAKE CHANNEL AT OLD DOG CAMP Weather Station, Observation :4.450, DS_ID :205125, Thing_ID :202150
```
#### 7. Delete Entities
The following sample shows how to delete an ObservedProperty and a Thing.
```
#!C#
Service srvc = new Service("http://chashuhotpot.sensorup.com/OGCSensorThings/v1.0");
TextBox1.Text += srvc.deleteObservedProperty(298786) + "\n------------------------------------------------------------------------------------\n";
TextBox1.Text += srvc.deleteThing(298893);
```
Sample Output:
```
Succeeded: Record is deleted
------------------------------------------------------------------------------------
Succeeded: Record is deleted
```
#### 8. Add Entity by JSON
The following sample shows how to add a Datastream entity by inserting JSON string parameter.
```
#!C#
Service srvc = new Service("http://chashuhotpot.sensorup.com/OGCSensorThings/v1.0");
string rsultStr = srvc.addNewDatastreamByJSON("{\"description\": \"Sample Description\", \"unitOfMeasurement\": {\"name\": \"degree Celsius\"," +
"\"symbol\": \"c\",\"definition\": \"Sample Definition\"},\"observationType\": \"Sample Type\",\"Thing\": {\"id\": 298904}," +
"\"Sensor\": {\"id\": 298803},\"ObservedProperty\": {\"id\": 298804}}");
TextBox1.Text = rsultStr;
```
Sample Output:
```
{"unitOfMeasurement":{"symbol":"c","name":"degree Celsius","definition":"Sample Definition"},
"observationType":"Sample Type","ObservedProperty":{"navigationLink":"../Datastreams(298914)/ObservedProperty"},
"description":"Sample Description","id":298914,"Observations":{"navigationLink":"../Datastreams(298914)/Observations"},
"Thing":{"navigationLink":"../Datastreams(298914)/Thing"},"selfLink":"http://localhost:8080/v1.0/Datastreams(298914)",
"Sensor":{"navigationLink":"../Datastreams(298914)/Sensor"}}
```
#### 9. Add Entities – Simple methods
The following sample shows how to add Location, Sensor, and FeatureOfInterest entities using simple methods. The outputs are written to a text box.
```
#!C#
Service srvc = new Service("http://chashuhotpot.sensorup.com/OGCSensorThings/v1.0");
//Ex1: Location
ArrayList coorAL = new ArrayList();
coorAL.Add(114.2314);
coorAL.Add(28.7754);
TextBox1.Text = srvc.addNewLocation("application/vnd.geo+json", "testing client library", "Point", coorAL, 167517);

//Ex2: Sensor
TextBox1.Text = srvc.addNewSensor("Sensor Test", "EncodingType_AB", "metadata.com");

//Ex3: FeatureOfInterest
TextBox1.Text = srvc.addNewFeatureOfInterest("FOI Description", "application/vnd.geo+json", "{\"coordinates\":[-125.231, 57.22314], \"type\": \"Point\"}");
```
Sample Output:
```
{"encodingType":"application/vnd.geo+json","Things":{"navigationLink":"../Locations(298923)/Things"},"description":"testing client library","location":{"coordinates":[114.2314,28.7754],"type":"Point"},"id":298923,"HistoricalLocations":{"navigationLink":"../Locations(298923)/HistoricalLocations"},"selfLink":"http://localhost:8080/v1.0/Locations(298923)"}

{"metadata":"metadata.com","Datastreams":{"navigationLink":"../Sensor(298925)/Datastreams"},"encodingType":"EncodingType_AB","description":"Sensor Test","id":298925,"selfLink":"http://localhost:8080/v1.0/Sensors(298925)"}

{"feature":{"coordinates":[-125.231,57.22314],"type":"Point"},"encodingType":"application/vnd.geo+json","description":"FOI Description","id":298926,"Observations":{"navigationLink":"../FeatureOfInterest(298926)/Observations"},"selfLink":"http://localhost:8080/v1.0/FeaturesOfInterest(298926)"}
```
#### 10. Update Entities
The following sample shows how to update Thing and Datastream entitiey contents.
```
#!C#
Service srvc = new Service("http://chashuhotpot.sensorup.com/OGCSensorThings/v1.0");

//Ex1: update Thing
TextBox1.Text += srvc.setThingDescription(298904, "New description now") + "\n";
TextBox1.Text += srvc.setThingProperties(298904, "\"sID\": \"12345\", \"color\":\"white\"") + "\n";
TextBox1.Text += srvc.setThingLocation(298904, 298919) + "\n";

//Ex2: update Datastream 
TextBox1.Text += srvc.setDatastreamDescription(298914, "New DS description now") + "\n";
TextBox1.Text += srvc.setDatastreamObservationType(298914, "new DS observationType now") + "\n";
TextBox1.Text += srvc.setDatastreamUnitOfMeasurement(298914, "\"symbol\":\"S\", \"name\":\"Sy\"");
```

For further description about each class and method, refer to the SensorThings .NET Client Library Documentation @  http://mro277002.bitbucket.org/SensorThingsClientLibraryDoc.





